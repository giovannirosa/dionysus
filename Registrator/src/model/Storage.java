package model;

import view.Screen;

public class Storage implements Model {
	
	private int id;
	private int item;
	private int quantity;
	private int category;
	private double total;

	public Storage(int i, int it, int q, int c) {
		id = i;
		item = it;
		quantity = q;
		category = c;
		setTotal(((Item)Screen.itemControl.get(it)).getValue()*q);
	}
	
	public Storage(String i, String it, String q, String c) {
		id = Integer.parseInt(i);
		item = Screen.itemControl.getId(it);
		quantity = Integer.parseInt(q);
		category = Screen.categoryControl.getId(c);
	}
	
	public Storage(int it, int q, int c) {
		item = it;
		quantity = q;
		category = c;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getDesc() {
		return Screen.itemControl.get(item).getDesc()+"/"+quantity+"/"+Screen.categoryControl.get(category).getDesc();
	}
}
