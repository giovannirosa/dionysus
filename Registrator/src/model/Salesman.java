package model;

public class Salesman extends User {
	public Salesman(String name, String pass) {
		setName(name);
		setPass(pass);
		setRole(Role.Salesman);
	}
	
	public Salesman(int id, String name, String pass) {
		setId(id);
		setName(name);
		setPass(pass);
		setRole(Role.Salesman);
	}
}
