package model;

public class Payment extends Sale {

	public Payment(int id, int it, int q, double t) {
		super(id, it, q, t);
	}
	
	public Payment(int it, int q, double t) {
		super(it, q, t);
	}
}
