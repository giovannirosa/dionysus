package model;

public class Item implements Model {
	private int id;
	private String name;
	private double value;
	private boolean combo;
	
	public Item(int i, String n, double v, boolean c) {
		id = i;
		name = n;
		value = v;
		combo = c;
	}
	
	public Item(String n, double v, boolean c) {
		name = n;
		value = v;
		combo = c;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getDesc() {
		return name;
	}

	public boolean isCombo() {
		return combo;
	}

	public void setCombo(boolean combo) {
		this.combo = combo;
	}
}
