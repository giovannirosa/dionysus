package model;

import view.Screen;

public class Sale implements Model {
	 
	private int id;
	private int item;
	private int quantity;
	private double total;
	
	public Sale(int i, int it, int q, double t) {
		id = i;
		item = it;
		quantity = q;
		setTotal(t);
	}
	
	public Sale(int it, int q, double t) {
		item = it;
		quantity = q;
		setTotal(t);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getDesc() {
		return Screen.itemControl.get(id).getDesc()+"/"+quantity;
	}
}
