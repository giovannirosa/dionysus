package model.table;

import java.text.DecimalFormat;

import javafx.beans.property.SimpleStringProperty;

public class TProcModel implements TableModel {
	private SimpleStringProperty item;
	private SimpleStringProperty quantity;
	private SimpleStringProperty total;
	
	public TProcModel(String it, int q, double t) {
		item = new SimpleStringProperty(it);
		quantity = new SimpleStringProperty(Integer.toString(q));
		total = new SimpleStringProperty(new DecimalFormat("##.00").format(t));
	}
	
	public TProcModel() {
		item = new SimpleStringProperty("");
		quantity = new SimpleStringProperty("");
		total = new SimpleStringProperty("");
	}

	public String getId() {
		if (item.get().isEmpty()) {
			return null;
		}
		return "";
	}

	public String getItem() {
		return item.get();
	}

	public void setItem(String item) {
		this.item.set(item);
	}

	public String getQuantity() {
		return quantity.get();
	}

	public void setQuantity(String quantity) {
		this.quantity.set(quantity);
	}

	public String getTotal() {
		return total.get();
	}

	public void setTotal(String total) {
		this.total.set(total);
	}
	
	public String getDesc() {
		return item.get()+"/"+quantity.get()+"/"+total.get();
	}
}
