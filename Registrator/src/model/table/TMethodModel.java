package model.table;

import java.text.DecimalFormat;

import javafx.beans.property.SimpleStringProperty;

public class TMethodModel implements TableModel {

	private SimpleStringProperty id;
	private SimpleStringProperty name;
	private SimpleStringProperty limit;
	
	public TMethodModel(int i, String n, double l) {
		id = new SimpleStringProperty(Integer.toString(i));
		name = new SimpleStringProperty(n);
		if (l==0)
			limit = new SimpleStringProperty("-");
		else
			limit = new SimpleStringProperty(new DecimalFormat("##.00").format(l));
	}
	
	public TMethodModel(int i, String n) {
		id = new SimpleStringProperty(Integer.toString(i));
		name = new SimpleStringProperty(n);
		limit = new SimpleStringProperty();
	}
	
	public TMethodModel() {
		id = new SimpleStringProperty("");
		name = new SimpleStringProperty("");
		limit = new SimpleStringProperty("");
	}

	public String getId() {
		return id.get();
	}

	public void setId(String id) {
		this.id.set(id);
	}

	public String getName() {
		return name.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public String getLimit() {
		return limit.get();
	}

	public void setLimit(String limit) {
		this.limit.set(limit);
	}

	public String getDesc() {
		return name.get();
	}
}
