package model.table;

import java.text.DecimalFormat;

import javafx.beans.property.SimpleStringProperty;

public class TVipModel implements TableModel {

	private SimpleStringProperty id;
	private SimpleStringProperty name;
	private SimpleStringProperty value;
	
	public TVipModel(int i, String n, double v) {
		id = new SimpleStringProperty(Integer.toString(i));
		name = new SimpleStringProperty(n);
		value = new SimpleStringProperty(new DecimalFormat("##.00").format(v));
	}
	
	public String getId() {
		return id.get();
	}
	
	public void setId(String id) {
		this.id.set(id);
	}
	
	public String getName() {
		return name.get();
	}
	
	public void setName(String name) {
		this.name.set(name);
	}
	
	public String getValue() {
		return value.get();
	}
	
	public void setValue(String value) {
		this.value.set(value);
	}
	
	public String getDesc() {
		return name.get();
	}
}
