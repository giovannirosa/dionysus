package model.table;

import javafx.beans.property.SimpleStringProperty;

public class TUserModel implements TableModel {

	private SimpleStringProperty id;
	private SimpleStringProperty user;
	private SimpleStringProperty pass;
	private SimpleStringProperty role;

	public TUserModel(int i, String n, String p, String r) {
		id = new SimpleStringProperty(Integer.toString(i));
		user = new SimpleStringProperty(n);
		pass = new SimpleStringProperty(p);
		role = new SimpleStringProperty(r);
	}
	
	public TUserModel() {
		id = new SimpleStringProperty("");
		user = new SimpleStringProperty("");
		pass = new SimpleStringProperty("");
		role = new SimpleStringProperty("");
	}
	
	public String getId() {
		return id.get();
	}

	public void setId(String id) {
		this.id.set(id);
	}
	
	public String getUser() {
		return user.get();
	}

	public void setUser(String s) {
		user.set(s);
	}

	public String getPass() {
		return pass.get();
	}

	public void setPass(String pass) {
		this.pass.set(pass);
	}

	public String getRole() {
		return role.get();
	}

	public void setRole(String role) {
		this.role.set(role);
	}
	
	public String getDesc() {
		return user.get();
	}
}
