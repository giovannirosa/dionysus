package model.table;

import java.text.DecimalFormat;

import javafx.beans.property.SimpleStringProperty;

public class TSaleModel implements TableModel {
	
	private SimpleStringProperty id;
	private SimpleStringProperty item;
	private SimpleStringProperty quantity;
	private SimpleStringProperty total;
	private SimpleStringProperty value;
	
	public TSaleModel(int i, String it, int q, double t) {
		id = new SimpleStringProperty(Integer.toString(i));
		item = new SimpleStringProperty(it);
		quantity = new SimpleStringProperty(Integer.toString(q));
		total = new SimpleStringProperty(new DecimalFormat("##.00").format(t));
		value = new SimpleStringProperty();
	}
	
	public TSaleModel(int i, String it, int q, double t, double v) {
		id = new SimpleStringProperty(Integer.toString(i));
		item = new SimpleStringProperty(it);
		quantity = new SimpleStringProperty(Integer.toString(q));
		total = new SimpleStringProperty(new DecimalFormat("##.00").format(t));
		value = new SimpleStringProperty(new DecimalFormat("##.00").format(v));
	}
	
	public TSaleModel() {
		id = new SimpleStringProperty("");
		item = new SimpleStringProperty("");
		quantity = new SimpleStringProperty("");
		total = new SimpleStringProperty("");
		value = new SimpleStringProperty("");
	}

	public String getId() {
		return id.get();
	}

	public void setId(String id) {
		this.id.set(id);
	}

	public String getItem() {
		return item.get();
	}

	public void setItem(String item) {
		this.item.set(item);
	}

	public String getQuantity() {
		return quantity.get();
	}

	public void setQuantity(String quantity) {
		this.quantity.set(quantity);
	}

	public String getTotal() {
		return total.get();
	}

	public void setTotal(String total) {
		this.total.set(total);
	}
	
	public String getValue() {
		return value.get();
	}

	public void setValue(String value) {
		this.value.set(value);
	}

	public String getDesc() {
		String desc = item.get();
		if (value.get()!=null)
			desc += "/"+value.get();
		return desc+"/"+quantity.get()+"/"+total.get();
	}
}
