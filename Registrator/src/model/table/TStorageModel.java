package model.table;

import java.text.DecimalFormat;

import javafx.beans.property.SimpleStringProperty;

public class TStorageModel implements TableModel {

	private SimpleStringProperty id;
	private SimpleStringProperty item;
	private SimpleStringProperty quantity;
	private SimpleStringProperty value;
	private SimpleStringProperty total;
	private SimpleStringProperty category;
	private boolean changed;
	
	public TStorageModel(int i, String it, int q, double v, String c) {
		id = new SimpleStringProperty(Integer.toString(i));
		item = new SimpleStringProperty(it);
		quantity = new SimpleStringProperty(Integer.toString(q));
		value = new SimpleStringProperty(new DecimalFormat("#0.00").format(v));
		total = new SimpleStringProperty(new DecimalFormat("#0.00").format(v*q));
		category = new SimpleStringProperty(c);
	}
	
	public TStorageModel() {
		id = new SimpleStringProperty("");
		item = new SimpleStringProperty("");
		quantity = new SimpleStringProperty("");
		value = new SimpleStringProperty("");
		total = new SimpleStringProperty("");
		category = new SimpleStringProperty("");
	}

	public String getId() {
		return id.get();
	}

	public void setId(String id) {
		this.id.set(id);
	}

	public String getItem() {
		return item.get();
	}

	public void setItem(String item) {
		this.item.set(item);
	}

	public String getQuantity() {
		return quantity.get();
	}

	public void setQuantity(String quantity) {
		this.quantity.set(quantity);
	}

	public String getValue() {
		return value.get();
	}

	public void setValue(String value) {
		this.value.set(value);
	}

	public String getTotal() {
		return total.get();
	}

	public void setTotal(String total) {
		this.total.set(total);
	}

	public boolean isChanged() {
		return changed;
	}

	public void setChanged(boolean changed) {
		this.changed = changed;
	}

	public String getCategory() {
		return category.get();
	}

	public void setCategory(String category) {
		this.category.set(category);
	}

	public String getDesc() {
		return item.get()+"/"+quantity.get()+"/"+value.get()+"/"+category.get();
	}
}
