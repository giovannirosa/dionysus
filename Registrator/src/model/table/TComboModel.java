package model.table;

import javafx.beans.property.SimpleStringProperty;

public class TComboModel implements TableModel {

	private SimpleStringProperty item;
	private SimpleStringProperty quantity;
	
	public TComboModel(String i, int q) {
		item = new SimpleStringProperty(i);
		quantity = new SimpleStringProperty(Integer.toString(q));
	}
	
	public TComboModel() {
		item = new SimpleStringProperty("");
		quantity = new SimpleStringProperty("");
	}

	public String getItem() {
		return item.get();
	}

	public void setItem(String item) {
		this.item.set(item);
	}

	public String getQuantity() {
		return quantity.get();
	}

	public void setQuantity(String quantity) {
		this.quantity.set(quantity);
	}

	@Override
	public String getId() {
		return item.get();
	}

	@Override
	public String getDesc() {
		return item.get()+"/"+quantity.get();
	}
}
