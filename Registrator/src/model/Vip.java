package model;

public class Vip implements Model {

	private int id;
	private String name;
	private double value;
	
	public Vip(int id, String name, double value) {
		this.id = id;
		this.name = name;
		this.value = value;
	}
	
	public Vip(String name, double value) {
		this.name = name;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String getDesc() {
		// TODO Auto-generated method stub
		return null;
	}
}
