package model;

public class Method implements Model {
	
	private int id;
	private String name;
	private double limit;
	
	public Method(int i, String m, double l) {
		id = i;
		name = m;
		setLimit(l);
	}
	
	public Method(int i, String m) {
		id = i;
		name = m;
	}
	
	public Method(String m, double l) {
		name = m;
		limit = l;
	}
	
	public Method(String m) {
		name = m;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String method) {
		this.name = method;
	}

	public double getLimit() {
		return limit;
	}

	public void setLimit(double limit) {
		this.limit = limit;
	}

	public String getDesc() {
		return name+"/"+limit;
	}
}
