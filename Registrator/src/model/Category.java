package model;

public class Category extends Method {

	public Category(int i, String m) {
		super(i, m);
	}
	
	public Category(String m) {
		super(m);
	}
	
	public String getDesc() {
		return getName();
	}
}
