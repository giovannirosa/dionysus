package model;

public class Admin extends User {
	public Admin(String name, String pass) {
		setName(name);
		setPass(pass);
		setRole(Role.Admin);
	}
	
	public Admin(int id, String name, String pass) {
		setId(id);
		setName(name);
		setPass(pass);
		setRole(Role.Admin);
	}
}
