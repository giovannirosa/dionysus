package model;

import view.utils.Factory;

public abstract class User implements Model {

	public enum Role {
		Admin, Salesman;
		
		public int index() {
			switch(this) {
			case Admin:
				return 1;
			case Salesman:
				return 2;
			}
			return 0;
		}
	}
	
	private int id;
	private String name;
	private String pass;
	private Role role;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void changePass(String oldPass, String newPass) {
		if (!pass.equals(oldPass)) {
			String log = "The old password is not correct!";
			Factory.log(log);
			return;
		}
		setPass(newPass);
	}
	
	public String getDesc() {
		return name;
	}
}
