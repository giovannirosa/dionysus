package view.utils;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import view.InitialView;
import view.Screen;

public class ConfigDialog extends Stage {
	
	VBox geralPane = new VBox(20);
	GridPane inputPane = new GridPane();
	HBox buttonPane = new HBox(20);
	
	Label partyLabel = new Label("Festa:");
	Label langLabel = new Label("Idioma:");
	
	TextField partyField = new TextField();
	ComboBox<String> langBox = new ComboBox<>();
	
	Button saveBut = new Button("Salvar",new ImageView(Factory.getButIcon("save.png")));
	Button printBut = new Button("Imprimir Relat�rio",new ImageView(Factory.getButIcon("printer.png")));
	Button resetBut = new Button("Resetar",new ImageView(Factory.getButIcon("clear.png")));
	Button cancelBut = new Button("Cancelar",new ImageView(Factory.getButIcon("exit.png")));
	
	public ConfigDialog() {
		this.setTitle("Configura��es");
		this.setResizable(false);
		this.initStyle(StageStyle.UTILITY);
		this.initOwner(Screen.getModal());
		this.initModality(Modality.APPLICATION_MODAL);
		
		buttonPane.setAlignment(Pos.CENTER);
		buttonPane.getChildren().addAll(saveBut,printBut,resetBut,cancelBut);
		
		setActions();
		
		inputPane.setAlignment(Pos.CENTER);
		inputPane.setHgap(20);
		inputPane.setVgap(20);
		inputPane.add(partyLabel, 0, 0);
		inputPane.add(partyField, 1, 0);
		inputPane.add(langLabel, 0, 1);
		inputPane.add(langBox, 1, 1);
		
		partyField.setText(Screen.partyControl.getParty());
		partyField.setId("party");
		langBox.getItems().add("Portugu�s");
		langBox.getSelectionModel().select(0);
		langBox.setId("party");
		
		geralPane.setAlignment(Pos.CENTER);
		geralPane.setPadding(new Insets(20));
		geralPane.getChildren().addAll(inputPane,buttonPane);
		geralPane.setPrefWidth(760);
		geralPane.setId("Pane");
		
		Scene scene = new Scene(geralPane);
		scene.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER)
				saveBut.fire();
			if (e.getCode() == KeyCode.ESCAPE)
				cancelBut.fire();
				
		});
		this.setScene(scene);
		scene.getStylesheets().addAll
		(Screen.class.getResource("/Style.css").toExternalForm(),
		Screen.class.getResource("/StyleComboBox.css").toExternalForm(),
		Screen.class.getResource("/StyleTable.css").toExternalForm(),
		Screen.class.getResource("/StyleScroll.css").toExternalForm());
		
		this.requestFocus();
	}

	private void setActions() {
		saveBut.setOnAction(e -> {
			if (!Screen.partyControl.getParty().equals(partyField.getText())) {
				Screen.partyControl.update(partyField.getText());
				String log = Screen.userControl.getUser()+" changed party!";
				Factory.log(log);
			}
			this.close();
		});
		printBut.setOnAction(e -> {
			if (!Factory.showConfirmation("Tem certeza que deseja imprimir o relat�rio com as informa��es atuais?"))
				return;
			
			Printer.printReport();
		});
		resetBut.setOnAction(e -> {
			if (!Factory.showConfirmation("Tem certeza que deseja resetar o sistema? Todos os dados ser�o apagados!"))
				return;
			
			Screen.saleControl.truncate();
			Screen.saleTab.refreshTable();
			
			Screen.storageControl.truncate();
			Screen.storageTab.refreshTable();
			
			Screen.payControl.truncate();
			Screen.payTab.refreshTable();
			
			InitialView.reloadTables();
			
			Screen.categoryControl.truncate();
			Screen.categoryTab.refreshTable();
			
			Screen.methodControl.truncate();
			Screen.methodTab.refreshTable();
			
			Screen.itemControl.truncate();
			Screen.itemTab.refreshTable();
			
			Screen.partyControl.reset();
			partyField.setText("");
			
			Factory.showWarning("Sistema resetado com sucesso!");
		});
		cancelBut.setOnAction(e -> this.close());
	}
}
