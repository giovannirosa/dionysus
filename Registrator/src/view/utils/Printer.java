package view.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.Item;
import model.Method;
import model.Model;
import model.Payment;
import model.Sale;
import model.Storage;
import view.Screen;

public class Printer {
	
	private static final String PRINT_PATH = Factory.DIONYSUS_PATH+"\\print.txt";
	private static final String REPORT_PATH = Factory.DIONYSUS_PATH+"\\report.txt";
	
	private static final String COMMAND_SALE = "senddat.exe "+PRINT_PATH+" USBPRN";
	private static final String COMMAND_REPORT = "senddat.exe "+REPORT_PATH+" USBPRN";
	
	private static final List<String> buffer = new ArrayList<>();
	private static int count;
	
	public static void clearBuffer() {
		buffer.clear();
		count = 0;
	}
	
	public static void addBuffer(String item) {
		String party = Screen.partyControl.getParty();
		LocalDateTime date = LocalDateTime.now();
		
		buffer.add("ESC @");
		buffer.add("GS ! 20");
		buffer.add("ESC a 1");
		buffer.add("------------------------ CR LF");
		buffer.add("GS ( L 6   0  48  69  32 32 1 1");
		buffer.add("ESC d 2");
		buffer.add('"'+Normalizer.normalize(item, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").toUpperCase()+'"'+" CR LF");
		buffer.add("GS ! 2");
		buffer.add("ESC d 2");
		buffer.add('"'+Normalizer.normalize(party, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").toUpperCase()+'"'+" CR LF");
		buffer.add("ESC d 2");
		buffer.add("GS ! 0");
		buffer.add('"'+date.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT))+'"'+" CR LF");
		buffer.add("GS ! 20");
		buffer.add("------------------------ CR LF");
		buffer.add("ESC d 2");
		buffer.add("ESC i");
		
		count++;
	}
	
	public static void printBuffer() {
		File f = new File(Factory.DIONYSUS_PATH+"\\print.txt");
		try {
			Files.write(f.toPath(), buffer, Charset.forName("UTF-8"));
			Runtime.getRuntime().exec(COMMAND_SALE);
			Thread.sleep(count * 1000);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static String format(String st, int size) {
		StringBuilder in = new StringBuilder(st);
		while (in.length() < size)
			in.append(" ");
		if (in.length() > size) {
			in.substring(14);
			in.append(" ");
		}
		return Normalizer.normalize(in, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").toUpperCase();
	}
	
	public static void printReport() {
		String party = Screen.partyControl.getParty();
		LocalDateTime date = LocalDateTime.now();
		
		List<String> saleItems = new ArrayList<>();
		List<String> saleUnitValues = new ArrayList<>();
		List<String> saleQuantities = new ArrayList<>();
		List<String> saleTotals = new ArrayList<>();
		double saleTotal = 0;
		for (Model m : Screen.saleControl.getMap().values()) {
			Sale o = (Sale) m;
			Item i = (Item) Screen.itemControl.get(o.getItem());
			saleItems.add(format(i.getName(), 15));
			saleUnitValues.add(format(new DecimalFormat("##.00").format(i.getValue()), 16));
			saleQuantities.add(format(Integer.toString(o.getQuantity()),8));
			saleTotals.add(new DecimalFormat("##.00").format(i.getValue()*o.getQuantity()));
			saleTotal += i.getValue()*o.getQuantity();
		}
		
		List<String> payMethods = new ArrayList<>();
		List<String> payLimits = new ArrayList<>();
		List<String> payQuantities = new ArrayList<>();
		List<String> payTotals = new ArrayList<>();
		double payTotal = 0;
		for (Model m : Screen.payControl.getMap().values()) {
			Payment o = (Payment) m;
			Method i = (Method) Screen.methodControl.get(o.getItem());
			payMethods.add(format(i.getName(), 15));
			payLimits.add(format(i.getLimit() != 0 ? new DecimalFormat("##.00").format(i.getLimit()) : "-", 16));
			payQuantities.add(format(Integer.toString(o.getQuantity()),8));
			payTotals.add(new DecimalFormat("##.00").format(o.getTotal()));
			payTotal += o.getTotal();
		}
		
		List<String> storageItems = new ArrayList<>();
		List<String> storageQuantities = new ArrayList<>();
		List<String> storageUnitValues = new ArrayList<>();
		List<String> storageTotals = new ArrayList<>();
		for (Model m : Screen.storageControl.getMap().values()) {
			Storage o = (Storage) m;
			Item i = (Item) Screen.itemControl.get(o.getItem());
			storageItems.add(format(i.getName(), 15));
			storageUnitValues.add(format(new DecimalFormat("##.00").format(i.getValue()),16));
			storageQuantities.add(format(Integer.toString(o.getQuantity()),8));
			storageTotals.add(new DecimalFormat("##.00").format(o.getTotal()));
		}
		
		List<String> lines = new ArrayList<>();
		lines.add("ESC @");
		lines.add("GS ! 20");
		lines.add("ESC a 1");
		lines.add("------------------------ CR LF");
		lines.add("GS ( L 6   0  48  69  32 32 1 1");
		lines.add("GS ! 2");
		lines.add("ESC d 2");
		lines.add('"'+Normalizer.normalize(party, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").toUpperCase()+'"'+" CR LF");
		lines.add("GS ! 0");
		lines.add("ESC d 2");
		lines.add('"'+date.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT))+'"'+" CR LF");
		lines.add("ESC d 2");
		lines.add('"'+"~~~~~~~~~~~~~~~~~~~~ VENDAS ~~~~~~~~~~~~~~~~~~~~"+'"'+" CR LF");
		lines.add("ESC d 2");
		lines.add("ESC a 0");
		lines.add("ESC E 1");
		lines.add('"'+"Item           Valor Un.(R$)   Qtd.    Total(R$)"+'"'+" CR LF");
		lines.add("ESC E 0");
		for (int i = 0; i < saleItems.size(); i++) {
			String saleItem = saleItems.get(i);
			String shorterItem = saleItem;
			if (saleItem.contains("/")) {
				shorterItem = shrink(saleItem);
			}
			lines.add('"'+format(shorterItem, 15)+saleUnitValues.get(i)+saleQuantities.get(i)+saleTotals.get(i)+'"'+" CR LF");
		}
		lines.add("\"                                      _________\" CR LF");
		lines.add('"'+"                            Soma(R$) = "+new DecimalFormat("##.00").format(saleTotal)+'"'+" CR LF");
		lines.add("ESC d 2");
		lines.add("ESC a 1");
		lines.add('"'+"~~~~~~~~~~~~~~ METODO DE PAGAMENTO ~~~~~~~~~~~~~"+'"'+" CR LF");
		lines.add("ESC d 2");
		lines.add("ESC a 0");
		lines.add("ESC E 1");
		lines.add('"'+"Metodo         Limite(R$)      Qtd.    Total(R$)"+'"'+" CR LF");
		lines.add("ESC E 0");
		for (int i = 0; i < payMethods.size(); i++)
			lines.add('"'+payMethods.get(i)+payLimits.get(i)+payQuantities.get(i)+payTotals.get(i)+'"'+" CR LF");
		lines.add("\"                                      _________\" CR LF");
		lines.add('"'+"                            Soma(R$) = "+new DecimalFormat("##.00").format(payTotal)+'"'+" CR LF");
		lines.add("ESC d 2");
		lines.add("ESC a 1");
		lines.add('"'+"~~~~~~~~~~~~~~ ESTOQUE RESTANTE ~~~~~~~~~~~~~~~~"+'"'+" CR LF");
		lines.add("ESC d 2");
		lines.add("ESC a 0");
		lines.add("ESC E 1");
		lines.add('"'+"Item           Valor Un.(R$)   Qtd."+'"'+" CR LF");
		lines.add("ESC E 0");
		for (int i = 0; i < storageItems.size(); i++)
			lines.add('"'+storageItems.get(i)+storageUnitValues.get(i)+storageQuantities.get(i)/*+totals3.get(i)*/+'"'+" CR LF");
		lines.add("ESC d 2");
		lines.add("ESC a 1");
		lines.add("GS ! 20");
		lines.add("------------------------ CR LF");
		lines.add("ESC d 2");
		lines.add("ESC i");
		
		File f = new File(Factory.DIONYSUS_PATH+"\\report.txt");
		try {
			Files.write(f.toPath(), lines, Charset.forName("UTF-8"));
			Runtime.getRuntime().exec(COMMAND_REPORT);
			Thread.sleep(1000);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
		Factory.log(Screen.userControl.getUser().getName()+" printed report at "+date.toString()+" !");
	}
	
	private static String shrink(String larger) {
		String shorter = "";
		String[] pieces = larger.split("/");
		List<String> list = Arrays.asList(pieces);
		for (String p : list) {
			shorter += p.substring(0, 4);
			if (list.indexOf(p) != list.size()-1)
				shorter += "/";
		}
		return shorter;
	}
}
