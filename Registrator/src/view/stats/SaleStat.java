package view.stats;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import model.Item;
import model.Model;
import model.Sale;
import view.Screen;

public class SaleStat extends Tab {
	
	VBox geralPane = new VBox(20);
	PieChart pieChart = new PieChart();
	final ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

	public SaleStat() {
		this.setContent(geralPane);
		this.setText("Vendas");
		
		pieChart.setData(pieChartData);
		refresh();
		
		geralPane.getChildren().add(pieChart);
	}
	
	private void refresh() {
		for (Model m : Screen.saleControl.getMap().values()) {
			Sale s = (Sale) m;
			Item i = (Item) Screen.itemControl.get(s.getItem());
			pieChartData.add(new PieChart.Data(i.getName(),s.getQuantity()));
		}
	}
}
