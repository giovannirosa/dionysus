package view.stats;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;

public class PayStat extends Tab {
	
	VBox geralPane = new VBox(20);
	PieChart pieChart = new PieChart();
	
	public PayStat() {
		this.setContent(geralPane);
		this.setText("Pagamentos");
//		Label l = new Label("Pagamentos");
//	    l.setRotate(45);
//	    StackPane stp = new StackPane(new Group(l));
//	    stp.setRotate(90);
//	    this.setGraphic(stp);
		
		ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                new PieChart.Data("Grapefruit", 13),
                new PieChart.Data("Oranges", 25),
                new PieChart.Data("Plums", 10),
                new PieChart.Data("Pears", 22),
                new PieChart.Data("Apples", 30));
		pieChart.setTitle("Pagamentos");
		pieChart.setData(pieChartData);
		
		geralPane.getChildren().add(pieChart);
	}
}
