package view;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import controller.UserControl;
import controller.database.DBUser;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.User;
import view.panes.FooterPane;
import view.panes.LogoPane;
import view.utils.Factory;

public class LoginView extends Stage {

	VBox geralPane = new VBox(20);
	GridPane gridPane = new GridPane();
	HBox buttonPane = new HBox(20);
	
	Label nameLabel = new Label("Usu�rio:");
	Label passLabel = new Label("Senha:");
	TextField nameField = new TextField();
	PasswordField passField = new PasswordField();
	Label capsLockLabel = new Label();
	
	LogoPane logoPane = new LogoPane(300,300);
	
	Button loginBut = new Button("Entrar",new ImageView(Factory.getButIcon("share.png")));
	Button closeBut = new Button("Sair",new ImageView(Factory.getButIcon("exit.png")));
	
	FooterPane markPane = new FooterPane();
	
	DBUser db = new DBUser();
	
	UserControl userControl = new UserControl();
	
	Scene scene = new Scene(geralPane);
	boolean capsOn;
	
	public LoginView() {
		this.setTitle("Dionysus v1.1");
		this.setResizable(false);
		
		LocalDateTime dt = LocalDateTime.now();
		Factory.log(dt.toString());

		Factory.setScreenResolution();

		logoPane.setAlignment(Pos.CENTER);
		
		capsOn = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);
		if (capsOn) {
			capsLockLabel.setText("CapsLock On");
		} else {
			capsLockLabel.setText("");
		}
		capsLockLabel.setId("mark");
		
		userControl.loadData();
		setActions();
		
		gridPane.setAlignment(Pos.CENTER);
		gridPane.setVgap(20);
		gridPane.setHgap(20);
		gridPane.add(nameLabel, 1, 1);
		gridPane.add(nameField, 2, 1);
		gridPane.add(passLabel, 1, 2);
		gridPane.add(passField, 2, 2);
		gridPane.add(capsLockLabel, 3, 2);
		
		buttonPane.getChildren().addAll(loginBut,closeBut);
		buttonPane.setAlignment(Pos.CENTER);
		
		geralPane.setAlignment(Pos.CENTER);
		geralPane.setPadding(new Insets(20));
		geralPane.getChildren().addAll(logoPane,gridPane,buttonPane,markPane);
		
		scene.getStylesheets().add
        (this.getClass().getResource("/Style.css").toExternalForm());
		
		this.setScene(scene);
		this.getIcons().add(new Image(this.getClass().getResource("/dionysus.png").toExternalForm()));
	}
	
	private void loginAction() {
		User u = (User) userControl.get(userControl.getId(nameField.getText()));
		if (u == null) {
			Factory.showWarning("No user with this name!");
			System.out.println("No user with this name!");
			return;
		}
		
		if (u.getPass().equals(passField.getText())) {
			userControl.setUser(u);
			String mess = u.getName()+" logged!";
			Factory.log(mess);
			this.close();
			new Screen("Dionysus v1.1",userControl).show();
		} else {
			Factory.showWarning("The password do not match!");
			System.out.println("The password do not match!");
		}
	}
	
	private void setActions() {
		scene.setOnKeyReleased( event -> {
			if ( event.getCode() == KeyCode.CAPS ) {
				capsOn = !capsOn;
				if (capsOn) {
					capsLockLabel.setText("CapsLock On");
				} else {
					capsLockLabel.setText("");
				}
			}
		});
		scene.setOnKeyPressed(e -> {
			if ( e.getCode() == KeyCode.ENTER ) {
				loginAction();
			}
		});
		loginBut.setOnAction(e -> loginAction());
		
		closeBut.setOnAction(e -> System.exit(0));
	}
}
