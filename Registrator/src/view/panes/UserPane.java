package view.panes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.User.Role;
import model.table.TUserModel;
import model.table.TableModel;
import view.Screen;
import view.table.UserTable;
import view.tabs.UserTab;
import view.utils.Factory;
import model.*;

public class UserPane extends UtilPane {
	Label idLabel = new Label("ID:");
	Label userLabel = new Label("User:");
	Label passwordLabel = new Label("Password:");
	Label typeLabel = new Label("Role:");
	
	TextField idField = new TextField();
	TextField userField = new TextField();
	PasswordField passwordField = new PasswordField();
	TextField passwordEditField = new TextField();
	ComboBox<String> typeBox = new ComboBox<>();
	
	public UserPane(UserTable table, UserTab tab) {
		super(table, tab);
	
		ObservableList<String> statOptions = FXCollections.observableArrayList(
				Role.Admin.toString(),
				Role.Salesman.toString());
		typeBox.setItems(statOptions);
		typeBox.getSelectionModel().select(0);
	}
	
	private boolean validate(boolean addMode) {
		if (userField.getText().isEmpty()) {
			Factory.showWarning("Please enter a user!");
			return false;
		}
		if (addMode) {
			if (passwordField.getText().isEmpty()) {
				Factory.showWarning("Please enter a password!");
				return false;
			}
		} else {
			if (passwordEditField.getText().isEmpty()) {
				Factory.showWarning("Please enter a password!");
				return false;
			}
		}
		
		return true;
	}
	
	public void addMode() {
		inputPane.getChildren().clear();
		inputPane.add(userLabel, 0, 0);
		inputPane.add(userField, 1, 0);
		inputPane.add(passwordLabel, 0, 1);
		inputPane.add(passwordField, 1, 1);
		inputPane.add(typeLabel, 0, 2);
		inputPane.add(typeBox, 1, 2);
		
		confirmBut.setOnAction(e -> {			
			if (!validate(true))
				return;
			
			User u;
			
			if (typeBox.getSelectionModel().getSelectedItem().equals(Role.Admin.toString())){
				u = new Admin(userField.getText(), passwordField.getText());
			} else {
				u = new Salesman(userField.getText(), passwordField.getText());
			}
			
			insert(u);
			String log = Screen.userControl.getUser().getName()+" added "+u.getDesc()+" to users!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void editMode(TableModel model) {
		TUserModel userModel = (TUserModel) model;
		idField.setText(model.getId());
		idField.setDisable(true);
		
		userField.setText(userModel.getUser());
		passwordEditField.setText(userModel.getPass());
		typeBox.getSelectionModel().select(userModel.getRole());
		
		inputPane.getChildren().clear();
		inputPane.add(idLabel, 0, 0);
		inputPane.add(idField, 1, 0);
		inputPane.add(userLabel, 0, 1);
		inputPane.add(userField, 1, 1);
		inputPane.add(passwordLabel, 0, 2);
		inputPane.add(passwordEditField, 1, 2);
		inputPane.add(typeLabel, 0, 3);
		inputPane.add(typeBox, 1, 3);
		
		confirmBut.setOnAction(e -> {			
			if (!validate(false))
				return;
			
			User u;
			
			if (typeBox.getSelectionModel().getSelectedItem().equals(Role.Admin.toString())){
				u = new Admin(Integer.parseInt(model.getId()),userField.getText(), passwordEditField.getText());
			} else {
				u = new Salesman(Integer.parseInt(model.getId()),userField.getText(), passwordEditField.getText());
			}
			
			update(u);
			String log = Screen.userControl.getUser().getName()+" updated "+u.getDesc()+" in users!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void clear() {
		userField.clear();
		passwordField.clear();
		typeBox.getSelectionModel().select(0);
	}
}
