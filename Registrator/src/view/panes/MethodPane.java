package view.panes;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import view.Screen;
import view.table.MethodTable;
import view.tabs.MethodTab;
import view.utils.Factory;
import model.*;
import model.table.TMethodModel;
import model.table.TableModel;

public class MethodPane extends UtilPane {
	Label idLabel = new Label("ID:");
	Label nameLabel = new Label("Nome:");
	Label limitLabel = new Label("Limite:");
	
	TextField idField = new TextField();
	TextField nameField = new TextField();
	TextField limitField = new TextField();
	
	public MethodPane(MethodTable table, MethodTab tab) {
		super(table,tab);
		setActions();
	}
	
	private boolean validate() {
		if (nameField.getText().isEmpty()) {
			Factory.showWarning("Please enter a name!");
			return false;
		}
		return true;
	}
	
	public void addMode() {
		inputPane.getChildren().clear();
		inputPane.add(nameLabel, 0, 0);
		inputPane.add(nameField, 1, 0);
		inputPane.add(limitLabel, 0, 1);
		inputPane.add(limitField, 1, 1);
		
		confirmBut.setOnAction(e -> {
			if (!validate())
				return;
			
			String limit = limitField.getText().replace(",", ".");
			Method i = new Method(nameField.getText(),Double.parseDouble(limit.isEmpty() ? "0" : limit));
			
			if (!insert(i))
				return;
			String log = Screen.userControl.getUser().getName()+" added "+i.getDesc()+" to methods!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void editMode(TableModel abstractModel) {
		TMethodModel model = (TMethodModel) abstractModel;
		idField.setText(model.getId());
		idField.setDisable(true);
		
		nameField.setText(model.getName());
		limitField.setText(model.getLimit());
		
		inputPane.getChildren().clear();
		inputPane.add(idLabel, 0, 0);
		inputPane.add(idField, 1, 0);
		inputPane.add(nameLabel, 0, 1);
		inputPane.add(nameField, 1, 1);
		inputPane.add(limitLabel, 0, 2);
		inputPane.add(limitField, 1, 2);
		
		confirmBut.setOnAction(e -> {
			if (!validate())
				return;
			
			String limit = limitField.getText().replace(",", ".");
			Method i = new Method(Integer.parseInt(model.getId()),nameField.getText(),
					Double.parseDouble(limit.isEmpty() ? "0" : limit));
			
			update(i);
			String log = Screen.userControl.getUser().getName()+" updated "+i.getDesc()+" in methods!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void clear() {
		nameField.clear();
		limitField.clear();
	}
	
	private void setActions() {
	    nameField.textProperty().addListener((obs,oldValue,newValue) -> {
	    	if (newValue.length() > 14)
	    		nameField.setText(oldValue);
	    });
	}
}
