package view.panes;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import view.Screen;
import view.table.CategoryTable;
import view.tabs.CategoryTab;
import view.utils.Factory;
import model.*;
import model.table.TCategoryModel;
import model.table.TableModel;

public class CategoryPane extends UtilPane {
	Label idLabel = new Label("ID:");
	Label userLabel = new Label("Name:");
	
	TextField idField = new TextField();
	TextField nameField = new TextField();
	
	public CategoryPane(CategoryTable table, CategoryTab tab) {
		super(table,tab);
	}
	
	private boolean validate() {
		if (nameField.getText().isEmpty()) {
			Factory.showWarning("Please enter a name!");
			return false;
		}
		return true;
	}
	
	public void addMode() {
		inputPane.getChildren().clear();
		inputPane.add(userLabel, 0, 0);
		inputPane.add(nameField, 1, 0);
		
		confirmBut.setOnAction(e -> {
			if (!validate())
				return;
			
			Category i = new Category(nameField.getText());
			
			insert(i);
			Screen.categoryControl.refreshBox(Screen.storageTab.getPane().categoryBox);
			String log = Screen.userControl.getUser().getName()+" added "+i.getDesc()+" to category!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void editMode(TableModel abstractModel) {
		TCategoryModel model = (TCategoryModel) abstractModel;
		idField.setText(model.getId());
		idField.setDisable(true);
		
		nameField.setText(model.getName());
		
		inputPane.getChildren().clear();
		inputPane.add(idLabel, 0, 0);
		inputPane.add(idField, 1, 0);
		inputPane.add(userLabel, 0, 1);
		inputPane.add(nameField, 1, 1);
		
		confirmBut.setOnAction(e -> {
			if (!validate())
				return;
			
			Category i = new Category(Integer.parseInt(model.getId()),nameField.getText());
			
			update(i);
			Screen.categoryControl.refreshBox(Screen.storageTab.getPane().categoryBox);
			String log = Screen.userControl.getUser().getName()+" updated "+i.getDesc()+" in category!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void clear() {
		nameField.clear();
	}
}
