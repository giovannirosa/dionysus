package view.panes;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import view.table.SaleTable;
import view.tabs.AbstractTab;
import model.table.TableModel;

public class SalePane extends UtilPane {
	Label userLabel = new Label("Name:");
	Label valueLabel = new Label("Value:");
	
	TextField nameField = new TextField();
	TextField itemField = new TextField();
	
	public SalePane(SaleTable table, AbstractTab tab) {
		super(table,tab);
		setActions();
	}
	
	public void clear() {
		nameField.clear();
		itemField.clear();
	}
	
	private void setActions() {
		// force the field to be numeric only
	    itemField.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	            if (!newValue.matches("\\d,*")) {
	            	itemField.setText(newValue.replaceAll("[^\\d,]", ""));
	            }
	        }
	    });
	}

	@Override
	public void addMode() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void editMode(TableModel model) {
		// TODO Auto-generated method stub
		
	}
		
}
