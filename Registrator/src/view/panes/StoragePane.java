package view.panes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import view.InitialView;
import view.Screen;
import view.table.StorageTable;
import view.tabs.StorageTab;
import view.utils.Factory;
import model.*;
import model.table.TStorageModel;
import model.table.TableModel;

public class StoragePane extends UtilPane {
	Label idLabel = new Label("ID:");
	Label itemLabel = new Label("Item:");
	Label quantityLabel = new Label("Quantidade:");
	Label categoryLabel = new Label("Categoria:");
	
	TextField idField = new TextField();
	public ComboBox<String> itemBox = new ComboBox<>();
	Spinner<Integer> valueSpin = new Spinner<>();
	public ComboBox<String> categoryBox = new ComboBox<>();
	
	public StoragePane(StorageTable table, StorageTab tab) {
		super(table, tab);

		Screen.itemControl.refreshBoxCombo(itemBox);
		
		SpinnerValueFactory<Integer> svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 0);
		valueSpin.setValueFactory(svf);
		valueSpin.setEditable(true);
		TextFormatter<Integer> formatter = new TextFormatter<>(svf.getConverter(), svf.getValue());
		valueSpin.getEditor().setTextFormatter(formatter);
		svf.valueProperty().bindBidirectional(formatter.valueProperty());
		
		ObservableList<String> categoryOptions = FXCollections.observableArrayList();
		for (Model i : Screen.categoryControl.getMap().values()) {
			categoryOptions.add(((Category)i).getName());
		}
		categoryBox.setItems(categoryOptions);
		categoryBox.getSelectionModel().select(0);
	}
	
	public void addMode() {
		inputPane.getChildren().clear();
		inputPane.add(itemLabel, 0, 0);
		inputPane.add(itemBox, 1, 0);
		inputPane.add(quantityLabel, 0, 1);
		inputPane.add(valueSpin, 1, 1);
		inputPane.add(categoryLabel, 0, 2);
		inputPane.add(categoryBox, 1, 2);
		
		confirmBut.setOnAction(e -> {
			String name = itemBox.getSelectionModel().getSelectedItem();
			String cat = categoryBox.getSelectionModel().getSelectedItem();
			
			int id = Screen.itemControl.getId(name);
			for (TableModel model : table.getItems()) {
				TStorageModel st = (TStorageModel) model;
				int id2 = Screen.itemControl.getId(st.getItem());
				if (id == id2) {
					Factory.showWarning("Voc� n�o pode adicionar o mesmo produto ao estoque mais de uma vez!\n"
							+ "Por favor edite o registro existente.");
					return;
				}
			}
			
			Storage i = new Storage(id,valueSpin.getValue(),
					Screen.categoryControl.getId(cat));
			
			insert(i);
			InitialView.reloadTables();
			InitialView.procView.reloadTables();
			String log = Screen.userControl.getUser().getName()+" added "+i.getDesc()+" to storage!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void editMode(TableModel abstractModel) {
		TStorageModel model = (TStorageModel) abstractModel;
		idField.setText(model.getId());
		idField.setDisable(true);
		
		itemBox.getSelectionModel().select(model.getItem());
		valueSpin.getValueFactory().setValue(Integer.parseInt(model.getQuantity()));
		categoryBox.getSelectionModel().select(model.getCategory());
		
		inputPane.getChildren().clear();
		inputPane.add(idLabel, 0, 0);
		inputPane.add(idField, 1, 0);
		inputPane.add(itemLabel, 0, 1);
		inputPane.add(itemBox, 1, 1);
		inputPane.add(quantityLabel, 0, 2);
		inputPane.add(valueSpin, 1, 2);
		inputPane.add(categoryLabel, 0, 3);
		inputPane.add(categoryBox, 1, 3);
		
		confirmBut.setOnAction(e -> {
			String name = itemBox.getSelectionModel().getSelectedItem();
			String cat = categoryBox.getSelectionModel().getSelectedItem();
			
			Storage i = new Storage(Integer.parseInt(model.getId()),Screen.itemControl.getId(name),valueSpin.getValue(),
					Screen.categoryControl.getId(cat));
			
			update(i);
			InitialView.reloadTables();
			InitialView.procView.reloadTables();
			String log = Screen.userControl.getUser().getName()+" updated "+i.getDesc()+" in storage!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void clear() {
		itemBox.getSelectionModel().select(0);
		valueSpin.getValueFactory().setValue(0);
		categoryBox.getSelectionModel().select(0);
	}
}
