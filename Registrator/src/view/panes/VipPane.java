package view.panes;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import view.Screen;
import view.table.VipTable;
import view.tabs.VipTab;
import view.utils.Factory;
import model.Vip;
import model.table.TVipModel;
import model.table.TableModel;

public class VipPane extends UtilPane {
	Label idLabel = new Label("ID:");
	Label userLabel = new Label("Nome:");
	Label valueLabel = new Label("Valor:");
	
	TextField idField = new TextField();
	TextField nameField = new TextField();
	TextField valueField = new TextField();
	
	public VipPane(VipTable table, VipTab tab) {
		super(table,tab);
		setActions();
	}
	
	private boolean validate() {
		if (nameField.getText().isEmpty()) {
			Factory.showWarning("Please enter a name!");
			return false;
		}
		if (valueField.getText().isEmpty()) {
			Factory.showWarning("Please enter a value!");
			return false;
		}
		return true;
	}
	
	public void addMode() {
		inputPane.getChildren().clear();
		inputPane.add(userLabel, 0, 0);
		inputPane.add(nameField, 1, 0);
		inputPane.add(valueLabel, 0, 1);
		inputPane.add(valueField, 1, 1);
		
		confirmBut.setOnAction(e -> {			
			if (!validate())
				return;
			
			Vip i = new Vip(nameField.getText(),Double.parseDouble(valueField.getText().replace(",", ".")));
			
			insert(i);
			String log = Screen.userControl.getUser().getName()+" added "+i.getName()+" to vip!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void editMode(TableModel abstractModel) {
		TVipModel model = (TVipModel) abstractModel;
		idField.setText(model.getId());
		idField.setDisable(true);
		
		nameField.setText(model.getName());
		valueField.setText(model.getValue());
		
		inputPane.getChildren().clear();
		inputPane.add(idLabel, 0, 0);
		inputPane.add(idField, 1, 0);
		inputPane.add(userLabel, 0, 1);
		inputPane.add(nameField, 1, 1);
		inputPane.add(valueLabel, 0, 2);
		inputPane.add(valueField, 1, 2);
		
		confirmBut.setOnAction(e -> {			
			if (!validate())
				return;
			
			Vip i = new Vip(Integer.parseInt(model.getId()),nameField.getText(),
					Double.parseDouble(valueField.getText().replace(",", ".")));
			
			update(i);
			String log = Screen.userControl.getUser().getName()+" updated "+i.getName()+" in vip!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void clear() {
		nameField.clear();
		valueField.clear();
	}
	
	private void setActions() {
		// force the field to be numeric only
	    valueField.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	            if (!newValue.matches("\\d,*")) {
	            	valueField.setText(newValue.replaceAll("[^\\d,]", ""));
	            }
	        }
	    });
	}
}
