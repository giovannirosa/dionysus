package view.panes;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import view.Screen;
import view.table.ItemTable;
import view.tabs.ItemTab;
import view.utils.Factory;
import model.*;
import model.table.TItemModel;
import model.table.TableModel;

public class ItemPane extends UtilPane {
	Label idLabel = new Label("ID:");
	Label userLabel = new Label("Name:");
	Label valueLabel = new Label("Value:");
	
	TextField idField = new TextField();
	TextField nameField = new TextField();
	TextField valueField = new TextField();
	
	public ItemPane(ItemTable table, ItemTab tab) {
		super(table,tab);
		setControl(Screen.itemControl);
		setActions();
	}
	
	private boolean validate() {
		if (nameField.getText().isEmpty()) {
			Factory.showWarning("Please enter a name!");
			return false;
		}
		if (valueField.getText().isEmpty()) {
			Factory.showWarning("Please enter a value!");
			return false;
		}
		return true;
	}
	
	public void addMode() {
		inputPane.getChildren().clear();
		inputPane.add(userLabel, 0, 0);
		inputPane.add(nameField, 1, 0);
		inputPane.add(valueLabel, 0, 1);
		inputPane.add(valueField, 1, 1);
		
		confirmBut.setOnAction(e -> {			
			if (!validate())
				return;
			
			Item i = new Item(nameField.getText(),Double.parseDouble(valueField.getText().replace(",", ".")),false);
			
			insert(i);
			Screen.itemControl.refreshBoxCombo(Screen.storageTab.getPane().itemBox);
			Screen.itemControl.refreshBoxCombo(Screen.itemTab.getComboPane().itemBox);
			String log = Screen.userControl.getUser().getName()+" added "+i.getDesc()+" to items!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void editMode(TableModel abstractModel) {
		TItemModel model = (TItemModel) abstractModel;
		idField.setText(model.getId());
		idField.setDisable(true);
		
		nameField.setText(model.getName());
		valueField.setText(model.getValue());
		
		inputPane.getChildren().clear();
		inputPane.add(idLabel, 0, 0);
		inputPane.add(idField, 1, 0);
		inputPane.add(userLabel, 0, 1);
		inputPane.add(nameField, 1, 1);
		inputPane.add(valueLabel, 0, 2);
		inputPane.add(valueField, 1, 2);
		
		confirmBut.setOnAction(e -> {			
			if (!validate())
				return;
			
			Item i = new Item(Integer.parseInt(model.getId()),nameField.getText(),
					Double.parseDouble(valueField.getText().replace(",", ".")),model.isCombo());
			
			update(i);
			Screen.itemControl.refreshBoxCombo(Screen.storageTab.getPane().itemBox);
			Screen.itemControl.refreshBoxCombo(Screen.itemTab.getComboPane().itemBox);
			String log = Screen.userControl.getUser().getName()+" updated "+i.getDesc()+" in items!";
			Factory.log(log);
			
			this.close();
		});
	}
	
	public void clear() {
		nameField.clear();
		valueField.clear();
	}
	
	private void setActions() {
		// force the field to be numeric only
	    valueField.textProperty().addListener((obs,oldValue,newValue) -> {
            if (!newValue.matches("\\d,*"))
            	valueField.setText(newValue.replaceAll("[^\\d,]", ""));
	    });
	    nameField.textProperty().addListener((obs,oldValue,newValue) -> {
	    	if (newValue.length() > 14)
	    		nameField.setText(oldValue);
	    });
	}
}
