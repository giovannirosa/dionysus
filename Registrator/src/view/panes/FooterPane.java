package view.panes;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class FooterPane extends VBox {

	Label markLabel = new Label("Dionysus");
	Label subLabel = new Label("The ultimate program for sales!");
	Label reserveLabel = new Label("\u00A9 All rights reserved");
	
	public FooterPane() {
		this.setAlignment(Pos.CENTER);
		this.getChildren().addAll(markLabel,subLabel,reserveLabel);
		this.setId("mark");
	}
	
}
