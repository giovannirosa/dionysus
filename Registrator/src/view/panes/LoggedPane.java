package view.panes;

import controller.UserControl;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class LoggedPane extends GridPane {

	Label userLabel1 = new Label("Usu�rio:");
	Label userLabel2 = new Label();
	Label roleLabel1 = new Label("Fun��o:");
	Label roleLabel2 = new Label();
	
	UserControl userControl = new UserControl();
	
	public LoggedPane() {
		userLabel2.setText(userControl.getUser().getName());
		roleLabel2.setText(userControl.getUser().getRole().toString());
		
		this.setId("logged");
		this.setHgap(5);
		this.setVgap(5);
		this.setAlignment(Pos.CENTER);
		this.setPadding(new Insets(5));
		this.add(userLabel1, 0, 0);
		this.add(userLabel2, 1, 0);
		this.add(roleLabel1, 0, 1);
		this.add(roleLabel2, 1, 1);
	}
}
