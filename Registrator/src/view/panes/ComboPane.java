package view.panes;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import model.Item;
import model.table.TComboModel;
import model.table.TableModel;
import view.Screen;
import view.table.ComboTable;
import view.table.ItemTable;
import view.tabs.ItemTab;
import view.utils.Factory;

public class ComboPane extends UtilPane {
	
	VBox actPane = new VBox(10);
	HBox midPane = new HBox(20);
	HBox tablePane = new HBox(20);
	HBox valuePane = new HBox(20);
	
	Label itemLabel = new Label("Item:");
	Label qtdLabel = new Label("Quantidade:");
	Label valueLabel = new Label("Valor:");
	
	public ComboBox<String> itemBox = new ComboBox<>();
	Spinner<Integer> valueSpin = new Spinner<>();
	TextField valueField = new TextField();
	
	Button insertBut = new Button(">>");
	Button removeBut = new Button("<<");
	
	ComboTable comboTable = new ComboTable();

	public ComboPane(ItemTable table, ItemTab tab) {
		super(table, tab);
		setControl(Screen.itemControl);
		
		setActions();
		
		Screen.itemControl.refreshBoxCombo(itemBox);
		
		SpinnerValueFactory<Integer> svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1000, 0);
		valueSpin.setValueFactory(svf);
		valueSpin.setEditable(true);
		TextFormatter<Integer> formatter = new TextFormatter<>(svf.getConverter(), svf.getValue());
		valueSpin.getEditor().setTextFormatter(formatter);
		svf.valueProperty().bindBidirectional(formatter.valueProperty());
		
		inputPane.add(itemLabel, 0, 0);
		inputPane.add(itemBox, 1, 0);
		inputPane.add(qtdLabel, 0, 1);
		inputPane.add(valueSpin, 1, 1);
		
		actPane.setAlignment(Pos.CENTER);
		actPane.getChildren().addAll(insertBut,removeBut);
		
		tablePane.getChildren().add(comboTable);
		HBox.setHgrow(comboTable, Priority.ALWAYS);
		comboTable.getData().add(new TComboModel());
		comboTable.setPrefWidth(400);
		
		midPane.setAlignment(Pos.CENTER);
		midPane.getChildren().addAll(inputPane,actPane,tablePane);
		
		valuePane.setAlignment(Pos.CENTER);
		valuePane.getChildren().addAll(valueLabel,valueField);
		
		geralPane.getChildren().clear();
		geralPane.getChildren().addAll(midPane,valuePane,buttonPane);
		geralPane.setPrefWidth(1000);
	}
	
	private void setActions() {
		insertBut.setOnAction(e -> {
			if (comboTable.getItems().get(0).getId().isEmpty()) {
				comboTable.getItems().remove(0);
			}
			
			comboTable.getData().add(new TComboModel(itemBox.getSelectionModel().getSelectedItem(), valueSpin.getValue()));
		});
		removeBut.setOnAction(e -> {
			TComboModel item = (TComboModel) comboTable.getSelectionModel().getSelectedItem();
			if (comboTable.getItems().get(0).getId() == null) {
				Factory.showWarning("N�o existem itens para serem removidos!");
				return;
			}
			if (item == null) {
				Factory.showWarning("Por favor selecione um item ao lado para remover!");
				return;
			}
			if (item.getId().isEmpty()) {
				Factory.showWarning("N�o existem itens para serem removidos!");
				return;
			}
			
			comboTable.getData().remove(item);
			
			if (comboTable.getItems().size() < 1) {
				comboTable.getItems().add(new TComboModel());
			}
		});
		confirmBut.setOnAction(e -> {
			if (comboTable.getItems().get(0).getId().isEmpty()) {
				Factory.showWarning("Por favor selecione ao menos um item para formar o combo!");
				return;
			}
			
			StringBuilder item = new StringBuilder();
			for (TableModel m : comboTable.getItems()) {
				TComboModel c = (TComboModel) m;
				
				if (!(item.length() == 0))
					item.append("/");
				item.append(c.getQuantity()+c.getItem());
			}
			
			Item i = new Item(item.toString(),Double.parseDouble(valueField.getText().replace(",", ".")),true);
			
			insert(i);
			Screen.itemControl.refreshBoxCombo(Screen.storageTab.getPane().itemBox);
			Screen.itemControl.refreshBoxCombo(Screen.itemTab.getComboPane().itemBox);
			System.out.println(Screen.userControl.getUser().getName()+" added "+i.getDesc()+" to items!");
			
			this.close();
		});
		// force the field to be numeric only
	    valueField.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	            if (!newValue.matches("\\d,*")) {
	            	valueField.setText(newValue.replaceAll("[^\\d,]", ""));
	            }
	        }
	    });
	}

	@Override
	public void addMode() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void editMode(TableModel model) {
		// TODO Auto-generated method stub
		
	}

	public void clear() {
		itemBox.getSelectionModel().select(0);
		valueSpin.getValueFactory().setValue(0);
	}
}
