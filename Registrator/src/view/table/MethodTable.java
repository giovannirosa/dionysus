package view.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import model.table.TableModel;

public class MethodTable extends Table<TableModel> {
	private TableColumn<TableModel, String> idCol = new TableColumn<>("Id");
	private TableColumn<TableModel, String> nameCol = new TableColumn<>("Nome");
	private TableColumn<TableModel, String> limitCol = new TableColumn<>("Limite");
	
	@SuppressWarnings("unchecked")
	public MethodTable() {
		getData().clear();
		this.getColumns().addAll(nameCol,limitCol);
		this.setItems(getData());
		
		idCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("id"));
		nameCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("name"));
		limitCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("limit"));
		
		configColumns();
	}
}
