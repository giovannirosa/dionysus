package view.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import model.table.TableModel;

public class ProcessTable extends Table<TableModel> {
	private TableColumn<TableModel, String> itemCol = new TableColumn<>("Item");
	private TableColumn<TableModel, String> quantityCol = new TableColumn<>("Quantidade");
	private TableColumn<TableModel, String> totalCol = new TableColumn<>("Pre�o Total (R$)");
	
	@SuppressWarnings("unchecked")
	public ProcessTable() {
		getData().clear();
		this.getColumns().addAll(itemCol,quantityCol,totalCol);
		this.setItems(getData());
		
		itemCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("item"));
		quantityCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("quantity"));
		totalCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("total"));
		
		configColumns();
	}
}
