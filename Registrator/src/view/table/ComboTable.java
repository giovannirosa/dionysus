package view.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import model.table.TableModel;

public class ComboTable extends Table<TableModel> {
	private TableColumn<TableModel, String> itemCol = new TableColumn<>("Item");
	private TableColumn<TableModel, String> quantityCol = new TableColumn<>("Quantidade");
	
	@SuppressWarnings("unchecked")
	public ComboTable() {
		getData().clear();
		this.getColumns().addAll(itemCol,quantityCol);
		this.setItems(getData());
		
		itemCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("item"));
		quantityCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("quantity"));
		
		configColumns();
	}
}
