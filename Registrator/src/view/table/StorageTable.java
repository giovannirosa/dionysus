package view.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import model.table.TableModel;

public class StorageTable extends Table<TableModel> {
	private TableColumn<TableModel, String> idCol = new TableColumn<>("ID");
	private TableColumn<TableModel, String> itemCol = new TableColumn<>("Item");
	private TableColumn<TableModel, String> quantityCol = new TableColumn<>("Quantidade");
	private TableColumn<TableModel, String> valueCol = new TableColumn<>("Pre�o Unit�rio (R$)");
	private TableColumn<TableModel, String> totalCol = new TableColumn<>("Valor Total (R$)");
	private TableColumn<TableModel, String> catCol = new TableColumn<>("Categoria");
	
	@SuppressWarnings("unchecked")
	public StorageTable() {
		getData().clear();
		this.getColumns().addAll(itemCol,valueCol,quantityCol,totalCol,catCol);
		this.setItems(getData());
		
		idCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("id"));
		itemCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("item"));
		quantityCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("quantity"));
		valueCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("value"));
		catCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("category"));
		totalCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("total"));
		
		configColumns();
	}
}
