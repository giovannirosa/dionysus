package view.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import model.table.TableModel;

public class SaleTable extends Table<TableModel> {
	private TableColumn<TableModel, String> idCol = new TableColumn<>("ID");
	protected TableColumn<TableModel, String> itemCol = new TableColumn<>("Item");
	private TableColumn<TableModel, String> valueCol = new TableColumn<>("Pre�o Unit�rio (R$)");
	protected TableColumn<TableModel, String> quantityCol = new TableColumn<>("Quantidade");
	private TableColumn<TableModel, String> totalCol = new TableColumn<>("Valor Total (R$)");
	
	@SuppressWarnings("unchecked")
	public SaleTable() {
		getData().clear();
		this.getColumns().addAll(itemCol,valueCol,quantityCol,totalCol);
		this.setItems(getData());
		
		idCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("id"));
		itemCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("item"));
		quantityCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("quantity"));
		totalCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("total"));
		valueCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("value"));
		
		configColumns();
	}
}
