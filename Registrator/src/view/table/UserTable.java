package view.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import model.table.TableModel;

public class UserTable extends Table<TableModel> {
	private TableColumn<TableModel, String> idCol = new TableColumn<>("Id");
	private TableColumn<TableModel, String> userCol = new TableColumn<>("Usu�rio");
	private TableColumn<TableModel, String> passCol = new TableColumn<>("Senha");
	private TableColumn<TableModel, String> roleCol = new TableColumn<>("Fun��o");
	
	@SuppressWarnings("unchecked")
	public UserTable() {
		getData().clear();
		this.getColumns().addAll(userCol,passCol,roleCol);
		this.setItems(getData());
		
		idCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("id"));
		userCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("user"));
		passCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("pass"));
		roleCol.setCellValueFactory(new PropertyValueFactory<TableModel,String>("role"));
		
		configColumns();
	}
}
