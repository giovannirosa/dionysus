package view.tabs;

import model.table.TableModel;
import view.Screen;
import view.panes.VipPane;
import view.table.VipTable;
import view.utils.Factory;

public class VipTab extends AbstractTab {
	
	VipTable vipTable = new VipTable();
	VipPane vipPane = new VipPane(vipTable,this);
	
	public VipTab() {
		super("Vip",Screen.vipControl);
		setTable(vipTable);
		setAddPane(vipPane);
		vipPane.setControl(Screen.vipControl);
		initUI();
		setActions();
		Screen.vipControl.loadTable(vipTable.getData());
	}
	
	private void setActions() {
		addBut.setOnAction(e -> {
			vipPane.addMode();
			vipPane.clear();
			addPane();
		});
		editBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			vipPane.editMode(vipTable.getSelectionModel().getSelectedItem());
			addPane();
		});
		delBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			TableModel model = table.getSelectionModel().getSelectedItem();
			
			delete(model);
			
			String log = Screen.userControl.getUser().getName()+" removed "+model.getDesc()+" from vip!";
			Factory.log(log);
		});
	}
}
