package view.tabs;

import model.table.TStorageModel;
import model.table.TableModel;
import view.InitialView;
import view.Screen;
import view.panes.StoragePane;
import view.table.StorageTable;
import view.utils.Factory;

public class StorageTab extends AbstractTab {
	
	StorageTable storageTable = new StorageTable();
	StoragePane storagePane = new StoragePane(storageTable,this);
	
	public StorageTab() {
		super("Estoque",Screen.storageControl);
		storagePane.setControl(Screen.storageControl);
		setTable(storageTable);
		setAddPane(storagePane);
		initUI();
		setActions();
		Screen.storageControl.loadTable(storageTable.getData());
	}
	
	public StoragePane getPane() {
		return storagePane;
	}
	
	private void setActions() {
		addBut.setOnAction(e -> {
			storagePane.addMode();
			storagePane.clear();
			addPane();
		});
		editBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			storagePane.editMode(storageTable.getSelectionModel().getSelectedItem());
			addPane();
		});
		delBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			TableModel model = storageTable.getSelectionModel().getSelectedItem();
			
			if (!delete(model))
				return;
			
			if (storageTable.getItems().size() < 1)
				storageTable.getData().add(new TStorageModel());
			
			String log = Screen.userControl.getUser().getName()+" removed "+model.getDesc()+" record from storage!";
			Factory.log(log);
			
			InitialView.reloadTables();
		});
	}
}
