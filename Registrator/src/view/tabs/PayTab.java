package view.tabs;

import model.table.TSaleModel;
import model.table.TableModel;
import view.InitialView;
import view.Screen;
import view.panes.SalePane;
import view.table.PayTable;
import view.utils.Factory;

public class PayTab extends AbstractTab {
	
	PayTable payTable = new PayTable();
	SalePane payPane = new SalePane(payTable,this);
	
	public PayTab() {
		super("Pagamentos",Screen.payControl);
		payPane.setControl(Screen.payControl);
		setTable(payTable);
		setAddPane(payPane);
		initUI();
		setActions();
		Screen.payControl.loadTable(payTable.getData());
	}
	
	private void setActions() {
		addBut.setOnAction(e -> {
			Factory.showWarning("N�o � poss�vel adicionar Pagamento!\nPor favor use o bot�o Vender!");
		});
		editBut.setOnAction(e -> {
			Factory.showWarning("N�o � poss�vel editar Pagamento!\nPor favor exclua e use o bot�o Vender!");
		});
		delBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			TableModel model = payTable.getSelectionModel().getSelectedItem();
			
			if (!delete(model))
				return;
			
			if (payTable.getItems().size() < 1)
				payTable.getData().add(new TSaleModel());
			
			String log = Screen.userControl.getUser().getName()+" removed "+model.getDesc()+" sale from sales!";
			Factory.log(log);
			
			InitialView.reloadTables();
		});
	}
}
