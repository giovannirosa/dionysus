package view.tabs;

import model.table.TMethodModel;
import model.table.TableModel;
import view.Screen;
import view.panes.MethodPane;
import view.table.MethodTable;
import view.utils.Factory;

public class MethodTab extends AbstractTab {

	MethodTable methodTable = new MethodTable();
	MethodPane methodPane = new MethodPane(methodTable,this);
	
	public MethodTab() {
		super("M�todos",Screen.methodControl);
		methodPane.setControl(Screen.methodControl);
		setTable(methodTable);
		setAddPane(methodPane);
		initUI();
		setActions();
		Screen.methodControl.loadTable(methodTable.getData());
	}
	
	private void setActions() {
		clearBut.setOnAction(e -> {
			if (control.getMap().isEmpty()) {
				Factory.showWarning("Esta tabela j� est� vazia!");
				return;
			}
			
			if (!Screen.payControl.getMap().isEmpty()) {
				Factory.showWarning("Por favor limpe a tabela de Pagamentos antes!");
				return;
			}
			
			truncate();
		});
		addBut.setOnAction(e -> {
			methodPane.addMode();
			methodPane.clear();
			addPane();
		});
		editBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			methodPane.editMode(methodTable.getSelectionModel().getSelectedItem());
			addPane();
		});
		delBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			TableModel model = methodTable.getSelectionModel().getSelectedItem();
			
			if (!delete(model))
				return;
			
			if (methodTable.getItems().size() < 1)
				methodTable.getData().add(new TMethodModel());
			
			String log = Screen.userControl.getUser().getName()+" removed "+model.getDesc()+" from items!";
			Factory.log(log);
		});
	}
}
