package view.tabs;

import model.table.TUserModel;
import model.table.TableModel;
import view.Screen;
import view.panes.UserPane;
import view.table.UserTable;
import view.utils.Factory;

public class UserTab extends AbstractTab {
	
	UserTable userTable = new UserTable();
	UserPane userPane = new UserPane(userTable,this);
	
	public UserTab() {
		super("Usu�rios",Screen.userControl);
		userPane.setControl(Screen.userControl);
		setTable(userTable);
		setAddPane(userPane);
		initUI();
		setActions();
		control.loadTable(userTable.getData());
	}
	
	private void setActions() {
		addBut.setOnAction(e -> {
			userPane.addMode();
			userPane.clear();
			addPane();
		});
		editBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			userPane.editMode(userTable.getSelectionModel().getSelectedItem());
			addPane();
		});
		delBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			TableModel model = userTable.getSelectionModel().getSelectedItem();
			
			if (!delete(model))
				return;
			
			if (userTable.getItems().size() < 1)
				userTable.getData().add(new TUserModel());
			
			String log = Screen.userControl.getUser().getName()+" removed "+model.getDesc()+" record from users!";
			Factory.log(log);
		});
	}
}
