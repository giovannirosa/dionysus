package view.tabs;

import model.table.TSaleModel;
import model.table.TableModel;
import view.InitialView;
import view.Screen;
import view.panes.SalePane;
import view.table.SaleTable;
import view.utils.Factory;

public class SaleTab extends AbstractTab {
	
	SaleTable saleTable = new SaleTable();
	SalePane salePane = new SalePane(saleTable,this);
	
	public SaleTab() {
		super("Vendas",Screen.saleControl);
		salePane.setControl(Screen.saleControl);
		setTable(saleTable);
		setAddPane(salePane);
		initUI();
		setActions();
		Screen.saleControl.loadTable(saleTable.getData());
	}
	
	private void setActions() {
		addBut.setOnAction(e -> {
			Factory.showWarning("N�o � poss�vel adicionar Venda!\nPor favor use o bot�o Vender!");
		});
		editBut.setOnAction(e -> {
			Factory.showWarning("N�o � poss�vel editar Venda!\nPor favor exclua e use o bot�o Vender!");
		});
		delBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			TableModel model = saleTable.getSelectionModel().getSelectedItem();
			
			if (!delete(model))
				return;
			
			if (saleTable.getItems().size() < 1)
				saleTable.getData().add(new TSaleModel());
			
			String log = Screen.userControl.getUser().getName()+" removed "+model.getDesc()+" sale from sales!";
			Factory.log(log);
			
			InitialView.reloadTables();
		});
	}
}
