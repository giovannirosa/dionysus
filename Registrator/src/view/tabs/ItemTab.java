package view.tabs;

import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import model.table.TItemModel;
import model.table.TableModel;
import view.Screen;
import view.panes.ComboPane;
import view.panes.ItemPane;
import view.table.ItemTable;
import view.utils.Factory;

@SuppressWarnings("unlikely-arg-type")
public class ItemTab extends AbstractTab {
	
	ItemTable itTable = new ItemTable();
	ItemPane itemPane = new ItemPane(itTable,this);
	ComboPane comboPane = new ComboPane(itTable,this);
	
	Button comboBut = new Button("Combo",new ImageView(Factory.getButIcon("combo.png")));
	
	public ItemTab() {
		super("Itens",Screen.itemControl);
		setTable(itTable);
		setAddPane(itemPane);
		initUI();
		buttonPane.getChildren().add(1, comboBut);
		setActions();
		Screen.itemControl.loadTable(itTable.getData());
	}
	
	public void removeAddPane() {
		geralPane.getChildren().remove(addPane);
	}
	
	public ComboPane getComboPane() {
		return comboPane;
	}
	
	private void setActions() {
		clearBut.setOnAction(e -> {
			if (control.getMap().isEmpty()) {
				Factory.showWarning("Esta tabela j� est� vazia!");
				return;
			}
			
			if (!Screen.storageControl.getMap().isEmpty() && !Screen.saleControl.getMap().isEmpty()) {
				Factory.showWarning("Por favor limpe as tabelas de Estoque e Vendas antes!");
				return;
			}
			
			if (!Screen.storageControl.getMap().isEmpty()) {
				Factory.showWarning("Por favor limpe a tabela de Estoque antes!");
				return;
			}
			
			if (!Screen.saleControl.getMap().isEmpty()) {
				Factory.showWarning("Por favor limpe a tabela de Vendas antes!");
				return;
			}
			
			truncate();
		});
		addBut.setOnAction(e -> {
			itemPane.addMode();
			itemPane.clear();
			addPane();
		});
		comboBut.setOnAction(e -> {
			if (itTable.getItems().get(0).getId().equals("")) {
				Factory.showWarning("Por favor adicione pelo menos um item antes de montar um combo!");
				return;
			}
			comboPane = new ComboPane(itTable,this);
			comboPane.showAndWait();
		});
		editBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			itemPane.editMode(itTable.getSelectionModel().getSelectedItem());
			addPane();
		});
		delBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			TableModel model = table.getSelectionModel().getSelectedItem();
			
			if (!delete(model))
				return;
			
			if (itTable.getItems().size() < 1)
				itTable.getData().add(new TItemModel());
			
			Screen.itemControl.refreshBox(Screen.storageTab.getPane().itemBox);
			Screen.itemControl.refreshBoxCombo(comboPane.itemBox);
			String log = Screen.userControl.getUser().getName()+" removed "+model.getDesc()+" from items!";
			Factory.log(log);
		});
	}
}
