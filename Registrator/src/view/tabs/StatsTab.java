package view.tabs;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.VBox;
import view.panes.HeaderPane;
import view.stats.PayStat;
import view.stats.SaleStat;
import view.utils.StretchedTabPane;

public class StatsTab extends Tab {
	
	VBox geralPane = new VBox(20);
	
	HeaderPane headerPane = new HeaderPane();
	
	public StretchedTabPane tabPane = new StretchedTabPane();

	public StatsTab() {
		this.setContent(geralPane);
		this.setText("Estatísticas");
		
		tabPane.getTabs().add(new SaleStat());
		tabPane.getTabs().add(new PayStat());
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabPane.setId("Pane");
		
		geralPane.setAlignment(Pos.TOP_CENTER);
		geralPane.setPadding(new Insets(20));
		geralPane.getChildren().addAll(headerPane,tabPane);
	}
}
