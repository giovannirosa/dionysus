package view.tabs;

import model.table.TCategoryModel;
import model.table.TableModel;
import view.Screen;
import view.panes.CategoryPane;
import view.table.CategoryTable;
import view.utils.Factory;

public class CategoryTab extends AbstractTab {

	CategoryTable categoryTable = new CategoryTable();
	CategoryPane categoryPane = new CategoryPane(categoryTable,this);
	
	public CategoryTab() {
		super("Categorias",Screen.categoryControl);
		categoryPane.setControl(Screen.categoryControl);
		setTable(categoryTable);
		setAddPane(categoryPane);
		initUI();
		setActions();
		Screen.categoryControl.loadTable(categoryTable.getData());
	}
	
	private void setActions() {
		clearBut.setOnAction(e -> {
			if (control.getMap().isEmpty()) {
				Factory.showWarning("Esta tabela j� est� vazia!");
				return;
			}
			
			if (!Screen.payControl.getMap().isEmpty()) {
				Factory.showWarning("Por favor limpe a tabela de Estoque antes!");
				return;
			}
			
			truncate();
		});
		addBut.setOnAction(e -> {
			categoryPane.addMode();
			categoryPane.clear();
			addPane();
		});
		editBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			categoryPane.editMode(categoryTable.getSelectionModel().getSelectedItem());
			addPane();
		});
		delBut.setOnAction(e -> {
			if (!checkSelection())
				return;
			
			TableModel model = categoryTable.getSelectionModel().getSelectedItem();
			
			if (!delete(model))
				return;
			
			if (categoryTable.getItems().size() < 1)
				categoryTable.getData().add(new TCategoryModel());
			
			Screen.categoryControl.refreshBox(Screen.storageTab.getPane().categoryBox);
			String log = Screen.userControl.getUser().getName()+" removed "+model.getDesc()+" from items!";
			Factory.log(log);
		});
	}
}
