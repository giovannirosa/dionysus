package view;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import model.Model;

public class PayView extends VBox {

	GridPane gridPane = new GridPane();
	FlowPane methodPane = new FlowPane(20,20);
	VBox lowerPane = new VBox(20);
	VBox upperPane = new VBox(20);
	
	Label priceLabel = new Label("Pre�o Total:");
	Label methodLabel = new Label("M�todo:");
	
	
	TextField priceField = new TextField();
	
	public PayView() {
		
		lowerPane.setAlignment(Pos.CENTER_LEFT);
		lowerPane.getChildren().addAll(methodLabel,methodPane);
		
		gridPane.setAlignment(Pos.CENTER_LEFT);
		gridPane.setVgap(20);
		gridPane.setHgap(20);
		gridPane.add(priceLabel, 0, 0);
		gridPane.add(priceField, 2, 0);
		
		upperPane.setAlignment(Pos.CENTER_LEFT);
		upperPane.getChildren().addAll(gridPane,lowerPane);
		
		buildMethodPane();
		methodPane.setId("Pane");
		methodPane.setPadding(new Insets(20));
		methodPane.setAlignment(Pos.CENTER);
		methodPane.setOrientation(Orientation.VERTICAL);
		methodPane.setPrefWrapLength(200);
		methodPane.setPrefHeight(160);
		
		this.setSpacing(20);
		this.setAlignment(Pos.TOP_CENTER);
		this.setPadding(new Insets(20));
		this.getChildren().addAll(upperPane);
	}
	
	private void buildMethodPane() {
		methodPane.getChildren().clear();
		for (Model i : Screen.methodControl.getMap().values()) {
			Button b = new Button(i.getDesc());
			b.setOnAction(e -> {
				
			});
			b.setId("procBut");
			methodPane.getChildren().add(b);
		}
	}
}
