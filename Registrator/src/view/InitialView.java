package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import view.panes.HeaderPane;
import view.table.PayTable;
import view.table.SaleTable;
import view.table.StorageTable;
import view.utils.Factory;
import view.utils.StretchedTabPane;

public class InitialView extends Tab {
	
	VBox geralPane = new VBox(20);
	HBox userPane = new HBox(20);
	HBox saleLabelPane = new HBox(20);
	VBox salesPane = new VBox(5);
	HBox storageLabelPane = new HBox(20);
	VBox storagePane = new VBox(5);
	HBox buttonPane = new HBox(20);
	HBox togglePane = new HBox();
	
	public StretchedTabPane tabPane = new StretchedTabPane();
	Tab salesTab = new Tab("Vendas");
	Tab storageTab = new Tab("Estoque");
	
	HeaderPane headerPane = new HeaderPane();
	
	public static ProcessView procView = new ProcessView();
	
	Label salesLabel = new Label("Vendas:");
	Label storageLabel = new Label("Estoque:");
	
	static SaleTable saleTable = new SaleTable();
	static PayTable payTable = new PayTable();
	static StorageTable storageTable = new StorageTable();
	
	final ToggleGroup group = new ToggleGroup();
	ToggleButton saleTog = new ToggleButton("Itens");
	ToggleButton payTog = new ToggleButton("Pagamentos");
	
	Button sellBut = new Button("Vender");
	
	ImageView sellImg = new ImageView(Factory.getButIcon("dashboard.png"));

	public InitialView() {
		setContent(geralPane);
		setText("Dionysus");
		setClosable(false);
		
		Screen.saleControl.loadTable(saleTable.getData());
		salesPane.setAlignment(Pos.CENTER);
		salesPane.getChildren().addAll(saleTable);
		
		saleTable.getColumns().remove(3);
		storageTable.getColumns().remove(3);
		
		setActions();
		
		saleTog.setToggleGroup(group);
		saleTog.setSelected(true);
		payTog.setToggleGroup(group);
		togglePane.setAlignment(Pos.CENTER_LEFT);
		togglePane.getChildren().addAll(saleTog,payTog);
		
		saleLabelPane.setAlignment(Pos.CENTER_LEFT);
		saleLabelPane.getChildren().addAll(salesLabel);
		
		Screen.payControl.loadTable(payTable.getData());
		
		Screen.storageControl.loadTable(storageTable.getData());
		storageLabelPane.setAlignment(Pos.CENTER_LEFT);
		storageLabelPane.getChildren().addAll(storageLabel);
		
		storagePane.setAlignment(Pos.CENTER);
		storagePane.getChildren().addAll(storageTable);
		storagePane.setPadding(new Insets(20));
		
		buttonPane.setAlignment(Pos.CENTER);
		buttonPane.getChildren().addAll(sellBut);
		sellBut.setGraphic(sellImg);
		sellBut.setId("sellBut");
		
		salesTab.setClosable(false);
		salesTab.setContent(saleTable);
		storageTab.setClosable(false);
		storageTab.setContent(storageTable);
		
		tabPane.getTabs().addAll(storageTab,salesTab);
		tabPane.setId("Pane");
		
		geralPane.setAlignment(Pos.TOP_CENTER);
		geralPane.setPadding(new Insets(20));
		geralPane.getChildren().addAll(headerPane,tabPane,buttonPane);
	}
	
	public static void reloadTables() {
		Screen.saleControl.loadTable(saleTable.getData());
		Screen.saleTab.refreshTable();
		Screen.payControl.loadTable(payTable.getData());
		Screen.payTab.refreshTable();
		Screen.storageControl.loadTable(storageTable.getData());
		Screen.storageTab.refreshTable();
	}
	
	public void returnView() {
		setContent(geralPane);
	}
	
	private void setActions() {
		sellBut.setOnAction(e -> {
			procView = new ProcessView();
			setContent(procView);
			
			String log = Screen.userControl.getUser().getName()+" entered selling section!";
			Factory.log(log);
		});
		group.selectedToggleProperty().addListener(e -> {
			if (group.getSelectedToggle() == saleTog && !salesPane.getChildren().contains(saleTable)) {
				salesPane.getChildren().remove(payTable);
				salesPane.getChildren().add(saleTable);
			} else if (group.getSelectedToggle() == payTog && !salesPane.getChildren().contains(payTable)) {
				salesPane.getChildren().remove(saleTable);
				salesPane.getChildren().add(payTable);
			}
		});
	}
}
