package view;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import controller.ProcessControl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import model.Combo;
import model.Item;
import model.Method;
import model.Model;
import model.Storage;
import model.table.TProcModel;
import model.table.TStorageModel;
import model.table.TableModel;
import view.panes.FooterPane;
import view.panes.HeaderPane;
import view.table.ProcessTable;
import view.table.StorageTable;
import view.utils.Factory;

public class ProcessView extends VBox {

	GridPane gridPane = new GridPane();
	HBox buttonPane = new HBox(20);
	FlowPane catPane = new FlowPane(20,20);
	VBox lowerPane = new VBox(5);
	HBox labelPane = new HBox(20);
	HBox finishPane = new HBox(60);
	HBox actPane = new HBox(60);
	
	HeaderPane headerPane = new HeaderPane();
	FooterPane markPane = new FooterPane();
	
	ProcessTable procTable = new ProcessTable();
	StorageTable storageTable = new StorageTable();
	
	Label stLabel = new Label("Estoque:");
	Label priceLabel = new Label("Pre�o Total:");
	Label methodLabel = new Label("M�todo:");
	
	TextField priceField = new TextField();
	ComboBox<String> methodBox = new ComboBox<>();
	
	double total;
	
	Button addBut = new Button("Adicionar", new ImageView(Factory.getButIcon("chart2.png",60)));
	Button finishBut = new Button("Finalizar", new ImageView(Factory.getButIcon("cloud.png")));
	Button cancelBut = new Button("Cancelar", new ImageView(Factory.getButIcon("exit.png")));
	Button backBut = new Button("VOLTAR");
	Button removeBut = new Button("REMOVER");
	
	public ProcessView() {
		
		setActions();
		
		ObservableList<String> methodOptions = FXCollections.observableArrayList();
		for (Model i : Screen.methodControl.getMap().values()) {
			methodOptions.add(((Method)i).getName());
		}
		methodBox.setItems(methodOptions);
		methodBox.getSelectionModel().select(0);
		
		backBut.setId("procBut");
		backBut.setOnAction(e -> {
			buildCatPane();
		});
		
		buildCatPane();
		catPane.setAlignment(Pos.CENTER);
		catPane.setPadding(new Insets(20));
		catPane.setOrientation(Orientation.HORIZONTAL);
		
		labelPane.setAlignment(Pos.CENTER_LEFT);
		labelPane.getChildren().add(stLabel);
		
		priceField.setDisable(true);
		priceField.setText("0,00");
		priceField.setId("sell");
		
		gridPane.setAlignment(Pos.CENTER_LEFT);
		gridPane.setVgap(20);
		gridPane.setHgap(20);
		gridPane.add(priceLabel, 0, 0);
		gridPane.add(priceField, 2, 0);
		gridPane.add(methodLabel, 3, 0);
		gridPane.add(methodBox, 4, 0);
		
		buttonPane.setAlignment(Pos.CENTER);
		buttonPane.getChildren().addAll(finishBut,cancelBut);
		finishBut.setId("sellBut");
		cancelBut.setId("sellBut");
		
		procTable.getItems().add(new TProcModel());
		Screen.storageControl.loadTable(storageTable.getData());
		
		lowerPane.setAlignment(Pos.CENTER);
		lowerPane.getChildren().addAll(labelPane,storageTable);
		
		actPane.setAlignment(Pos.CENTER);
		actPane.getChildren().addAll(addBut);
		addBut.setId("sellBut");
		
		finishPane.setAlignment(Pos.CENTER);
		finishPane.getChildren().addAll(gridPane,buttonPane);
	
		this.setSpacing(20);
		this.setAlignment(Pos.TOP_CENTER);
		this.setPadding(new Insets(20));
		this.getChildren().addAll(catPane,procTable,finishPane);
	}
	
	private void buildCatPane() {
		catPane.getChildren().clear();
		for (Model i : Screen.categoryControl.getMap().values()) {
			Button b = new Button(i.getDesc());
			b.setOnAction(e -> {
				buildItemPane(b);
			});
			b.setId("procBut");
			catPane.getChildren().add(b);
		}
		
		Button b = new Button("combos");
		b.setOnAction(e -> {
			buildComboPane(b);
		});
		b.setId("procBut");
		catPane.getChildren().add(b);
		
		removeBut.setId("procBut");
		catPane.getChildren().add(removeBut);
	}
	
	private void buildComboPane(Button b) {
		catPane.getChildren().clear();
		for (Model s : Screen.itemControl.getMap().values()) {
			Item it = (Item) s;
			if (it.isCombo()) {
				Button a = new Button(it.getName());
				a.setId("procBut");
				a.setOnAction(x -> addComboAction(it.getName()));
				catPane.getChildren().add(a);
			}
		}
		catPane.getChildren().add(backBut);
	}
	
	private void buildItemPane(Button b) {
		catPane.getChildren().clear();
		for (Model s : Screen.storageControl.getMap().values()) {
			Storage st = (Storage) s;
			if (st.getCategory() == Screen.categoryControl.getId(b.getText())) {
				String name = Screen.itemControl.get(((Storage)s).getItem()).getDesc();
				Button a = new Button(name);
				a.setId("procBut");
				a.setOnAction(x -> addAction(name));
				catPane.getChildren().add(a);
			}
		}
		catPane.getChildren().add(backBut);
	}
	
	private void addComboAction(String name) {
		String names[] = name.split("/");
		List<Combo> comboList = new ArrayList<>();
		for (String n : names)
			comboList.add(new Combo(Integer.parseInt(n.substring(0,1)),n.substring(1)));
		
		TStorageModel st = null;
		List<Integer> indexList = new ArrayList<>();
		for (Combo c : comboList) {
			for (int i = 0; i < storageTable.getItems().size(); i++) {
				st = (TStorageModel) storageTable.getItems().get(i);
				if (st.getItem().equals(c.getName())) {
					indexList.add(i);
					if (Integer.parseInt(st.getQuantity()) < 1) {
						Factory.showWarning("N�o existe mais "+st.getItem()+" no estoque!\nO combo n�o pode ser vendido!");
						return;
					}
				}
			}
		}
		
		if (procTable.getItems().get(0).getId()==null) {
			procTable.getItems().remove(0);
		}
		
		for (TableModel model2 : procTable.getItems()) {
			TProcModel saleModel = (TProcModel) model2;
			if (saleModel.getItem().equals(name)) {
				int q = Integer.parseInt(saleModel.getQuantity())+1;
				saleModel.setQuantity(Integer.toString(q));
				
				addTotal(saleModel,name);
				updateTotal();
				removeStorage(comboList);
				return;
			}
		}
		
		Item item = (Item) Screen.itemControl.get(Screen.itemControl.getId(name));
		TableModel newItem = new TProcModel(name,1,item.getValue());
		procTable.getItems().add(newItem);
		removeStorage(comboList);
		updateTotal();
	}
	
	private void addAction(String name) {
		TStorageModel item = null;
		for (TableModel model : storageTable.getItems()) {
			item = (TStorageModel) model;
			if (item.getItem().equals(name)) {
				break;
			}
		}
		
		if (Integer.parseInt(item.getQuantity()) < 1) {
			Factory.showWarning("N�o existe mais "+item.getItem()+" no estoque!");
			return;
		}
		
		if (procTable.getItems().get(0).getId()==null) {
			procTable.getItems().remove(0);
		}
		
		for (TableModel model : procTable.getItems()) {
			TProcModel saleModel = (TProcModel) model;
			if (saleModel.getItem().equals(item.getItem())) {
				int q = Integer.parseInt(saleModel.getQuantity())+1;
				saleModel.setQuantity(Integer.toString(q));
				
				addTotal(saleModel,item);
				updateTotal();
				removeStorage(item);
				return;
			}
		}


		TableModel newItem = new TProcModel(item.getItem(),1,
				Double.parseDouble(item.getValue().replace(",", ".")));
		procTable.getItems().add(newItem);
		removeStorage(item);
		updateTotal();
	}
	
	public void reloadTables() {
		Screen.storageControl.loadTable(storageTable.getData());
	}

	private void removeStorage(TStorageModel item) {
		item.setQuantity(Integer.toString(Integer.parseInt(item.getQuantity())-1));
		item.setChanged(true);
		refreshTables();
	}
	
	private void removeStorage(List<Combo> list) {
		for (Combo c : list) {
			for (TableModel model : storageTable.getItems()) {
				TStorageModel st = (TStorageModel) model;
				if (st.getItem().equals(c.getName())) {
					st.setQuantity(Integer.toString(Integer.parseInt(st.getQuantity())-c.getQuantity()));
					st.setChanged(true);
					break;
				}
			}
		}
		refreshTables();
	}
	
	private void refreshTables() {
		procTable.getColumns().get(0).setVisible(false);
		procTable.getColumns().get(0).setVisible(true);
		storageTable.getColumns().get(0).setVisible(false);
		storageTable.getColumns().get(0).setVisible(true);
	}
	
	private void updateTotal() {
		if (procTable.getItems().get(0).getId()==null) {
			priceField.setText("0,00");
			return;
		}
		
		total = 0;
		for (TableModel model : procTable.getItems()) {
			TProcModel p = (TProcModel) model;
			total += Double.parseDouble(p.getTotal().replace(",", "."));
		}
		
		priceField.setText(new DecimalFormat("##.00").format(total));
	}
	
	private void addTotal(TProcModel saleModel, TStorageModel item) {
		double t = Double.parseDouble(saleModel.getTotal().replace(",", "."));
		t += Double.parseDouble(item.getValue().replace(",", "."));
		saleModel.setTotal(new DecimalFormat("##.00").format(t));
	}
	
	private void addTotal(TProcModel saleModel, String n) {
		Item item = (Item) Screen.itemControl.get(Screen.itemControl.getId(n));
		double t = Double.parseDouble(saleModel.getTotal().replace(",", "."));
		t += item.getValue();
		saleModel.setTotal(new DecimalFormat("##.00").format(t));
	}
	
	private void subtractTotal(TProcModel item) {
		double t = Double.parseDouble(item.getTotal().replace(",", "."));
		int q = Integer.parseInt(item.getQuantity());
		double u = t/q;
		
		t -= u;
		item.setTotal(new DecimalFormat("##.00").format(t));
	}

	private void setActions() {
		removeBut.setOnAction(e -> removeAction());
		procTable.setRowFactory(new Callback<TableView<TableModel>, TableRow<TableModel>>() {
			@Override
			public TableRow<TableModel> call(TableView<TableModel> param) {
				TableRow<TableModel> row = new TableRow<TableModel>();
				row.setOnMouseClicked(new EventHandler<MouseEvent>() {

					@Override
					public void handle(MouseEvent event) {
						if ((event.getClickCount() == 2)) {
							removeAction();
						}
					}

				});
				return row;
			}
		});
		finishBut.setOnAction(e -> {
			if (procTable.getItems().get(0).getId() == null) {
				Factory.showWarning("Por favor selecione ao menos um item para venda!");
				return;
			}
			double limit = ((Method)Screen.methodControl.get(
					Screen.methodControl.getId(methodBox.getSelectionModel().getSelectedItem())))
					.getLimit();
			if (limit != 0 && total > limit) {
				Factory.showWarning("O valor total passou do limite de R$"
						+ new DecimalFormat("##.00").format(limit) +" para o m�todo "+methodBox.getSelectionModel().getSelectedItem()+"!\n"
						+ "Por favor reduza o valor total!");
				return;
			}
			if (Factory.showConfirmation("Deseja mesmo encerrar esta venda?")) {
				finishAction();
			}
		});
		cancelBut.setOnAction(e -> {
			Screen.initView.returnView();
		});
	}
	
	private void finishAction() {
		ProcessControl.processSale(procTable, storageTable,
				methodBox.getSelectionModel().getSelectedItem(), total);
		InitialView.reloadTables();
		Screen.initView.returnView();
	}
	
	private void removeAction() {
		TProcModel item = (TProcModel) procTable.getSelectionModel().getSelectedItem();
		if (procTable.getItems().get(0).getId() == null) {
			Factory.showWarning("N�o existem itens para serem removidos!");
			return;
		}
		if (item == null) {
			Factory.showWarning("Por favor selecione um item abaixo para remover!");
			return;
		}
		if (item.getId() == null) {
			Factory.showWarning("N�o existem itens para serem removidos!");
			return;
		}
		
		for (TableModel model : storageTable.getItems()) {
			TStorageModel stModel = (TStorageModel) model;
			if (stModel.getItem().equals(item.getItem())) {
				int q = Integer.parseInt(stModel.getQuantity())+1;
				stModel.setQuantity(Integer.toString(q));
				refreshTables();
			}
		}

		for (TableModel model : procTable.getItems()) {
			TProcModel saleModel = (TProcModel) model;
			if (saleModel.getItem().equals(item.getItem())) {
				int q = Integer.parseInt(saleModel.getQuantity())-1;
				if (q < 1) {
					procTable.getItems().remove(item);
					if (procTable.getItems().size() < 1) {
						procTable.getItems().add(new TProcModel());
					}
					updateTotal();
					subtractTotal(saleModel);
					return;
				}
				updateTotal();
				subtractTotal(saleModel);
				
				saleModel.setQuantity(Integer.toString(q));
				refreshTables();
				return;
			}
		}
	}
}
