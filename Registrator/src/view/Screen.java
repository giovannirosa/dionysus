package view;

import java.io.IOException;

import controller.CategoryControl;
import controller.ItemControl;
import controller.MethodControl;
import controller.PartyControl;
import controller.PayControl;
import controller.SaleControl;
import controller.StorageControl;
import controller.UserControl;
import controller.VipControl;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.User.Role;
import view.tabs.CategoryTab;
import view.tabs.ItemTab;
import view.tabs.MethodTab;
import view.tabs.PayTab;
import view.tabs.SaleTab;
import view.tabs.StatsTab;
import view.tabs.StorageTab;
import view.tabs.UserTab;
import view.tabs.VipTab;
import view.utils.ConfigDialog;
import view.utils.Factory;
import view.utils.HelpDialog;

public class Screen extends Stage {

	BorderPane geralPanel = new BorderPane();
	public static TabPane tabPane = new TabPane();
	
	public static UserControl userControl;
	public static ItemControl itemControl = new ItemControl();
	public static MethodControl methodControl = new MethodControl();
	public static StorageControl storageControl = new StorageControl();
	public static SaleControl saleControl = new SaleControl();
	public static PayControl payControl = new PayControl();
	public static VipControl vipControl = new VipControl();
	public static CategoryControl categoryControl = new CategoryControl();
	public static PartyControl partyControl = new PartyControl();
	
	public static InitialView initView;
	public static UserTab userTab;
	public static ItemTab itemTab;
	public static MethodTab methodTab;
	public static SaleTab saleTab;
	public static PayTab payTab;
	public static StorageTab storageTab;
	public static VipTab vipTab;
	public static CategoryTab categoryTab;
	public static StatsTab statsTab;
	public static ConfigDialog configDialog;
	public static HelpDialog helpDialog;

	MenuBar menuBar = new MenuBar();

	public MenuItem menuHelp = new MenuItem("Ajuda");
	public MenuItem menuExit = new MenuItem("Sair");
	Menu menuOptions = new Menu("Op��es");

	public MenuItem menuUser = new MenuItem("Usu�rios");
	public MenuItem menuItem = new MenuItem("Itens");
	public MenuItem menuMethod = new MenuItem("M�todos");
	public MenuItem menuSale = new MenuItem("Vendas");
	public MenuItem menuPay = new MenuItem("Pagamentos");
	public MenuItem menuStorage = new MenuItem("Estoque");
	public MenuItem menuCategory = new MenuItem("Categorias");
//	public MenuItem menuStats = new MenuItem("Estat�sticas");
	SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();
	public MenuItem menuConfig = new MenuItem("Configura��es");
	Menu menuAdmin = new Menu("Admin");
	
	public static Scene scene;

	private static Screen modal;
	
	public static Screen getModal() {
		return modal;
	}
	
	/**
	 * Build basic window components.
	 * @param title
	 * @throws IOException
	 */
	public Screen(String title, UserControl userControl) {
		modal = this;
		this.setTitle(title);
		this.setMaximized(true);
		this.setResizable(false);
		Screen.userControl = userControl;
		
		scene = new Scene(new VBox());
		
		menuUser.setAccelerator(new KeyCodeCombination(KeyCode.U, KeyCombination.SHORTCUT_DOWN));
		menuItem.setAccelerator(new KeyCodeCombination(KeyCode.I, KeyCombination.SHORTCUT_DOWN));
		menuMethod.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHORTCUT_DOWN));
		menuSale.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_DOWN));
		menuPay.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.SHORTCUT_DOWN));
		menuStorage.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN));
		menuCategory.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN));
//		menuStats.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN));
		menuConfig.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN));
		
		itemControl.loadData();
		methodControl.loadData();
		saleControl.loadData();
		payControl.loadData();
		storageControl.loadData();
		vipControl.loadData();
		categoryControl.loadData();
		
		buildTabs();
		setActions();

		geralPanel.setCenter(tabPane);
		
		if (userControl.getUser().getRole().equals(Role.Salesman)) {
			menuAdmin.setDisable(true);
		}

		menuOptions.getItems().addAll(menuHelp, menuExit);
		menuAdmin.getItems().addAll(menuUser,menuItem,menuCategory,menuStorage,
				menuMethod,menuSale,menuPay,/*menuStats,*/separatorMenuItem,menuConfig);

		menuBar.getMenus().addAll(menuAdmin,menuOptions);
		
		menuItem.setId("menu-item");
		
		tabPane.getTabs().add(initView);
		tabPane.setId("main-tab");

		((VBox)scene.getRoot()).getChildren().addAll(menuBar,geralPanel);
        this.setScene(scene);

        scene.getStylesheets().addAll
        (this.getClass().getResource("/Style.css").toExternalForm(),
        this.getClass().getResource("/StyleComboBox.css").toExternalForm(),
        this.getClass().getResource("/StyleTable.css").toExternalForm(),
        this.getClass().getResource("/StyleScroll.css").toExternalForm(),
        this.getClass().getResource("/StyleChart.css").toExternalForm());
        this.getIcons().add(new Image(this.getClass().getResource("/dionysus.png").toExternalForm()));
	}
	
	private void buildTabs() {
		userTab = new UserTab();
		itemTab = new ItemTab();
		methodTab = new MethodTab();
		saleTab = new SaleTab();
		payTab = new PayTab();
		storageTab = new StorageTab();
		initView = new InitialView();
		vipTab = new VipTab();
		categoryTab = new CategoryTab();
		statsTab = new StatsTab();
		configDialog = new ConfigDialog();
		helpDialog = new HelpDialog();
	}
	
	private void setActions() {
		menuUser.setOnAction(e -> {
			if (tabPane.getTabs().contains(userTab)) {
				tabPane.getSelectionModel().select(userTab);
				return;
			}
			tabPane.getTabs().add(userTab);
			tabPane.getSelectionModel().selectLast();
			
			Factory.log(userControl.getUser().getName()+" entered users section!");
		});
		menuItem.setOnAction(e -> {
			if (tabPane.getTabs().contains(itemTab)) {
				tabPane.getSelectionModel().select(itemTab);
				return;
			}
			tabPane.getTabs().add(itemTab);
			tabPane.getSelectionModel().selectLast();
			
			Factory.log(userControl.getUser().getName()+" entered items section!");
		});
		menuMethod.setOnAction(e -> {
			if (tabPane.getTabs().contains(methodTab)) {
				tabPane.getSelectionModel().select(methodTab);
				return;
			}
			tabPane.getTabs().add(methodTab);
			tabPane.getSelectionModel().selectLast();
			
			Factory.log(userControl.getUser().getName()+" entered methods section!");
		});
		menuSale.setOnAction(e -> {
			if (tabPane.getTabs().contains(saleTab)) {
				tabPane.getSelectionModel().select(saleTab);
				return;
			}
			tabPane.getTabs().add(saleTab);
			tabPane.getSelectionModel().selectLast();
			
			Factory.log(userControl.getUser().getName()+" entered sales section!");
		});
		menuStorage.setOnAction(e -> {
			if (tabPane.getTabs().contains(storageTab)) {
				tabPane.getSelectionModel().select(storageTab);
				return;
			}
			tabPane.getTabs().add(storageTab);
			tabPane.getSelectionModel().selectLast();
			
			Factory.log(userControl.getUser().getName()+" entered storage section!");
		});
		menuCategory.setOnAction(e -> {
			if (tabPane.getTabs().contains(categoryTab)) {
				tabPane.getSelectionModel().select(categoryTab);
				return;
			}
			tabPane.getTabs().add(categoryTab);
			tabPane.getSelectionModel().selectLast();
			
			Factory.log(userControl.getUser().getName()+" entered category section!");
		});
		menuPay.setOnAction(e -> {
			if (tabPane.getTabs().contains(payTab)) {
				tabPane.getSelectionModel().select(payTab);
				return;
			}
			tabPane.getTabs().add(payTab);
			tabPane.getSelectionModel().selectLast();
			
			Factory.log(userControl.getUser().getName()+" entered pay section!");
		});
//		menuStats.setOnAction(e -> {
//			if (tabPane.getTabs().contains(statsTab)) {
//				tabPane.getSelectionModel().select(statsTab);
//				return;
//			}
//			tabPane.getTabs().add(statsTab);
//			tabPane.getSelectionModel().selectLast();
//			
//			System.out.println(userControl.getUser().getName()+" entered stats section!");
//		});
		menuConfig.setOnAction(e -> {
			configDialog.showAndWait();
			
			Factory.log(userControl.getUser().getName()+" entered config section!");
		});
		menuHelp.setOnAction(e -> {
			helpDialog.showAndWait();
			
			Factory.log(userControl.getUser().getName()+" entered help section!");
		});
		menuExit.setOnAction(e -> {
			if (Factory.showConfirmation("Tem certeza que deseja sair?")) {
				Factory.log(userControl.getUser().getName()+" exited Dionysus!");
				
				System.exit(0);
			}
		});
	}
}
