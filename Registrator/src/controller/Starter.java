package controller;

import controller.database.Connector;

public class Starter {
	public static void main(String[] args) throws Exception {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> Connector.connectClose()));
		Connector.connectOpen();
		Dionysus.boot();
	}
}
