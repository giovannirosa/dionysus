package controller;

import controller.database.DBParty;

public class PartyControl {
	
	DBParty db = new DBParty();
	private String party;
	
	public PartyControl() {
		party = db.select();
	}
	
	public void update(String p) {
		party = p;
		db.update(party);
	}
	
	public void reset() {
		party = "";
		db.update(party);
	}
	
	public String getParty() {
		return party;
	}
}
