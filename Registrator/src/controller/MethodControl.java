package controller;

import java.util.HashMap;
import java.util.Map;

import controller.database.DBMethod;
import javafx.collections.ObservableList;
import model.Method;
import model.Model;
import model.table.TMethodModel;
import model.table.TableModel;

public class MethodControl extends Control {
	
	Map<Integer,Model> methodMap = new HashMap<>();
	DBMethod dbMethod = new DBMethod();
	
	public MethodControl() {
		setDatabase(dbMethod);
		setMap(methodMap);
	}
	
	public void loadTable(ObservableList<TableModel> data) {
		data.clear();
		if (methodMap.isEmpty()) {
			data.add(new TMethodModel());
			return;
		}
		for (Model m : methodMap.values()) {
			Method o = (Method) m;
			data.add(new TMethodModel(o.getId(), o.getName(), o.getLimit()));
		}
	}
}
