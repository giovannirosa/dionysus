package controller;

import java.util.HashMap;
import java.util.Map;

import controller.database.DBCategory;
import javafx.collections.ObservableList;
import model.Category;
import model.Model;
import model.table.TCategoryModel;
import model.table.TableModel;

public class CategoryControl extends Control {
	Map<Integer,Model> catMap = new HashMap<>();
	DBCategory dbCat = new DBCategory();
	
	public CategoryControl() {
		setDatabase(dbCat);
		setMap(catMap);
	}
	
	public void loadTable(ObservableList<TableModel> data) {
		data.clear();
		if (catMap.isEmpty()) {
			data.add(new TCategoryModel());
			return;
		}
		for (Model m : catMap.values()) {
			Category o = (Category) m;
			data.add(new TCategoryModel(o.getId(), o.getName()));
		}
	}
}
