package controller;

import java.util.HashMap;
import java.util.Map;

import controller.database.DBPay;
import javafx.collections.ObservableList;
import model.Method;
import model.Model;
import model.Payment;
import model.table.TSaleModel;
import model.table.TableModel;
import view.Screen;

public class PayControl extends Control {

	Map<Integer,Model> payMap = new HashMap<>();
	DBPay dbPay = new DBPay();
	
	public PayControl() {
		setDatabase(dbPay);
		setMap(payMap);
	}
	
	public void loadTable(ObservableList<TableModel> data) {		
		data.clear();
		if (payMap.isEmpty()) {
			data.add(new TSaleModel());
			return;
		}
		for (Model m : payMap.values()) {
			Payment o = (Payment) m;
			Method i = (Method) Screen.methodControl.get(o.getItem());
			data.add(new TSaleModel(o.getId(), i.getName(), o.getQuantity(), o.getTotal()));
		}
	}
}
