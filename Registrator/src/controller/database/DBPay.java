package controller.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import model.Model;
import model.Payment;

public class DBPay extends Database {
	public static final int ID_INDEX = 1;
	public static final int METHOD_INDEX = 2;
	public static final int QUANTITY_INDEX = 3;
	public static final int TOTAL_INDEX = 4;
	
	public DBPay() {
		table = "payment";
	}
	
	PreparedStatement insertStmt;
	public PreparedStatement getInsertStmt() throws SQLException{
    	if( insertStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			insertStmt  = conn.prepareStatement("INSERT INTO payment (method,quantity,total) VALUE (?, ?, ?)");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return insertStmt;
    }
	
	PreparedStatement updateStmt;
	public PreparedStatement getUpdateStmt() throws SQLException{
    	if( updateStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			updateStmt  = conn.prepareStatement("UPDATE payment SET method = ?, quantity = ?, total= ? WHERE id = ?");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return updateStmt;
    }
	
	public Map<Integer,Model> listAll() {
		ResultSet rs = null;
		Map<Integer,Model> PaymentMap = new HashMap<>();
		PreparedStatement stmt;
		try {
			stmt = getSelectStmt("payment");

			rs = stmt.executeQuery();
			while (rs.next()) {

				int id = rs.getInt(ID_INDEX);
				int method = rs.getInt(METHOD_INDEX);
				int quantity = rs.getInt(QUANTITY_INDEX);
				double total = rs.getInt(TOTAL_INDEX);

				PaymentMap.put(id,new Payment(id,method,quantity,total));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return PaymentMap;
	}
	
	public boolean insert(Model m) {
		Payment i = (Payment) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getInsertStmt();
			
			stmt.setInt(1, i.getItem());
			stmt.setInt(2, i.getQuantity());
			stmt.setDouble(3, i.getTotal());
			
			boo = stmt.executeUpdate() > 0;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean delete(int id) throws SQLException {
		PreparedStatement stmt = getDeleteStmt("payment");
		stmt.setInt(1, id);
		
		return stmt.executeUpdate() > 0;
	}

	@Override
	public int selectId(String n) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean update(Model m) {
		Payment i = (Payment) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getUpdateStmt();
			
			stmt.setInt(1, i.getItem());
			stmt.setInt(2, i.getQuantity());
			stmt.setDouble(3, i.getTotal());
			stmt.setInt(4, i.getId());
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean resetId() throws SQLException {
		PreparedStatement stmt = getResetIdStmt("payment");
		
		return stmt.executeUpdate() > 0;
	}
}
