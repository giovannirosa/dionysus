package controller.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import model.Admin;
import model.Model;
import model.Salesman;
import model.User;
import view.utils.Factory;

public class DBUser extends Database {
	
	public static final int ID_INDEX = 1;
	public static final int NAME_INDEX = 2;
	public static final int PASS_INDEX = 3;
	public static final int ROLE_INDEX = 4;
	
	public DBUser() {
		table = "users";
	}
	
	PreparedStatement insertStmt;
	public PreparedStatement getInsertStmt() throws SQLException{
    	if( insertStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			insertStmt  = conn.prepareStatement("INSERT INTO users (name,password,role) VALUE (?, ?, ?)");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return insertStmt;
    }
	
	PreparedStatement updateStmt;
	public PreparedStatement getUpdateStmt() throws SQLException{
    	if( updateStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			updateStmt  = conn.prepareStatement("UPDATE users SET name = ?, password = ?, role = ? WHERE id = ?");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return updateStmt;
    }
	
	public int selectId(String i) {
		ResultSet rs = null;
		try {
			PreparedStatement stmt = getSelectIdStmt("users");
			
			stmt.setString(1, i);
			
			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(ID_INDEX);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public boolean update(Model m) {
		User u = (User) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getUpdateStmt();
			
			stmt.setString(1, u.getName());
			stmt.setString(2, u.getPass());
			stmt.setInt(3, u.getRole().index());
			stmt.setInt(4, u.getId());
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public Map<Integer,Model> listAll() {
		ResultSet rs = null;
		Map<Integer,Model> userMap = new HashMap<>();
		PreparedStatement stmt;
		try {
			stmt = getSelectStmt("users");

			rs = stmt.executeQuery();
			while (rs.next()) {

				int id = rs.getInt(ID_INDEX);
				String user = rs.getString(NAME_INDEX);
				String pass = rs.getString(PASS_INDEX);
				int role = rs.getInt(ROLE_INDEX);
				User u;
				if (role==1)
					u = new Admin(id,user,pass);
				else
					u = new Salesman(id,user,pass);

				userMap.put(id,u);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userMap;
	}
	
	public boolean insert(Model m) {
		User u = (User) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getInsertStmt();
			
			stmt.setString(1, u.getName());
			stmt.setString(2, u.getPass());
			stmt.setInt(3, u.getRole().index());
			
			boo = stmt.executeUpdate() > 0;
			
		} catch (MySQLIntegrityConstraintViolationException d) {
			Factory.showWarning("Duplicated user! Please enter another name!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean delete(int id) throws SQLException {
		PreparedStatement stmt = getDeleteStmt("users");
		stmt.setInt(1, id);
		
		return stmt.executeUpdate() > 0;
	}
	
	public boolean resetId() throws SQLException {
		PreparedStatement stmt = getResetIdStmt("users");
		
		return stmt.executeUpdate() > 0;
	}
}
