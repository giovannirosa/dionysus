package controller.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import model.Model;
import model.Storage;

public class DBStorage extends Database {
	public static final int ID_INDEX = 1;
	public static final int ITEM_INDEX = 2;
	public static final int QUANTITY_INDEX = 3;
	public static final int CATEGORY_INDEX = 4;
	
	public DBStorage() {
		table = "storage";
	}

	PreparedStatement insertStmt;
	public PreparedStatement getInsertStmt() throws SQLException{
    	if( insertStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			insertStmt  = conn.prepareStatement("INSERT INTO storage (item,quantity,category) VALUE (?, ?, ?)");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return insertStmt;
    }
	
	PreparedStatement updateStmt;
	public PreparedStatement getUpdateStmt() throws SQLException{
    	if( updateStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			updateStmt  = conn.prepareStatement("UPDATE storage SET quantity = ?, category = ? WHERE id = ?");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return updateStmt;
    }
	
	public boolean update(Model m) {
		Storage i = (Storage) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getUpdateStmt();
			
			stmt.setInt(1, i.getQuantity());
			stmt.setInt(2, i.getCategory());
			stmt.setInt(3, i.getId());
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public Map<Integer,Model> listAll() {
		ResultSet rs = null;
		Map<Integer,Model> storageMap = new HashMap<>();
		PreparedStatement stmt;
		try {
			stmt = getSelectStmt("storage");

			rs = stmt.executeQuery();
			while (rs.next()) {

				int id = rs.getInt(ID_INDEX);
				int item = rs.getInt(ITEM_INDEX);
				int quantity = rs.getInt(QUANTITY_INDEX);
				int cat = rs.getInt(CATEGORY_INDEX);

				storageMap.put(id,new Storage(id,item,quantity,cat));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return storageMap;
	}
	
	public boolean insert(Model m) {
		Storage i = (Storage) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getInsertStmt();
			
			stmt.setInt(1, i.getItem());
			stmt.setInt(2, i.getQuantity());
			stmt.setInt(3, i.getCategory());
			
			boo = stmt.executeUpdate() > 0;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean delete(int id) throws SQLException {
		PreparedStatement stmt = getDeleteStmt("storage");
		stmt.setInt(1, id);
		
		return stmt.executeUpdate() > 0;
	}

	public int selectId(String n) {
		ResultSet rs = null;
		PreparedStatement stmt;
		try {
			stmt = getSelectIdStmt("storage");
			
			stmt.setString(1, n);
			
			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(ID_INDEX);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean resetId() throws SQLException {
		PreparedStatement stmt = getResetIdStmt("storage");
		
		return stmt.executeUpdate() > 0;
	}
}
