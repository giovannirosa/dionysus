package controller.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {

	private final static String url="jdbc:mysql://localhost:3306/reg?autoReconnect=true&useSSL=false";
	private final static String uname="root";
	private final static String pass="admin";
    
	private static Connection conn;
    
    public static void connectOpen() {
    	try {
    		if(conn == null){
    			conn = DriverManager.getConnection(url,uname,pass);
    		}
    		System.out.println("connection opened");
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
	}
    
    public static void connectClose() {
    	try {
			if (conn!=null && conn.isClosed())
				conn.close();
			System.out.println("connection closed");
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    public static Connection getConnection() {
    	return conn;
    }
}
