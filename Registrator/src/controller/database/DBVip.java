package controller.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import model.Vip;
import model.Model;

public class DBVip extends Database {
	public static final int ID_INDEX = 1;
	public static final int NAME_INDEX = 2;
	public static final int VALUE_INDEX = 3;
	
	PreparedStatement insertStmt;
	public PreparedStatement getInsertStmt() throws SQLException{
    	if( insertStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			insertStmt  = conn.prepareStatement("INSERT INTO vip (name,value) VALUE (?, ?)");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return insertStmt;
    }
	
	PreparedStatement updateStmt;
	public PreparedStatement getUpdateStmt() throws SQLException{
    	if( updateStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			updateStmt  = conn.prepareStatement("UPDATE vip SET name = ?, value = ? WHERE id = ?");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return updateStmt;
    }
	
	public int selectId(String n) {
		ResultSet rs = null;
		PreparedStatement stmt;
		try {
			stmt = getSelectIdStmt("vip");
			
			stmt.setString(1, n);
			
			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(ID_INDEX);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean update(Model m) {
		Vip i = (Vip) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getUpdateStmt();
			
			stmt.setString(1, i.getName());
			stmt.setDouble(2, i.getValue());
			stmt.setInt(3, i.getId());
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public Map<Integer,Model> listAll() {
		ResultSet rs = null;
		Map<Integer,Model> itemMap = new HashMap<>();
		PreparedStatement stmt;
		try {
			stmt = getSelectStmt("vip");

			rs = stmt.executeQuery();
			while (rs.next()) {

				int id = rs.getInt(ID_INDEX);
				String name = rs.getString(NAME_INDEX);
				double value = rs.getDouble(VALUE_INDEX);

				itemMap.put(id,new Vip(id,name,value));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return itemMap;
	}
	
	public boolean insert(Model m) {
		Vip i = (Vip) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getInsertStmt();
			
			stmt.setString(1, i.getName());
			stmt.setDouble(2, i.getValue());
			
			boo = stmt.executeUpdate() > 0;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean delete(int id) {
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getDeleteStmt("vip");
			
			stmt.setInt(1, id);
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean truncate() throws SQLException {
		PreparedStatement stmt = getTruncateStmt("vip");
		
		return stmt.executeUpdate() > 0;
	}
	
	public boolean resetId() throws SQLException {
		PreparedStatement stmt = getResetIdStmt("vip");
		
		return stmt.executeUpdate() > 0;
	}
}
