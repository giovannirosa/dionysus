package controller.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import model.Category;
import model.Model;

public class DBCategory extends Database {
	
	public static final int ID_INDEX = 1;
	public static final int NAME_INDEX = 2;
	
	public DBCategory() {
		table = "category";
	}
	
	PreparedStatement insertStmt;
	public PreparedStatement getInsertStmt() throws SQLException{
    	if( insertStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			insertStmt  = conn.prepareStatement("INSERT INTO category (name) VALUE (?)");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return insertStmt;
    }
	
	PreparedStatement updateStmt;
	public PreparedStatement getUpdateStmt() throws SQLException{
    	if( updateStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			updateStmt  = conn.prepareStatement("UPDATE category SET name = ? WHERE id = ?");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return updateStmt;
    }
	
	public boolean update(Model m) {
		Category i = (Category) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getUpdateStmt();
			
			stmt.setString(1, i.getName());
			stmt.setInt(2, i.getId());
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public Map<Integer,Model> listAll() {
		ResultSet rs = null;
		Map<Integer,Model> methodMap = new HashMap<>();
		PreparedStatement stmt;
		try {
			stmt = getSelectStmt("category");

			rs = stmt.executeQuery();
			while (rs.next()) {

				int id = rs.getInt(ID_INDEX);
				String name = rs.getString(NAME_INDEX);

				methodMap.put(id,new Category(id,name));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return methodMap;
	}
	
	public boolean insert(Model m) {
		Category i = (Category) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getInsertStmt();
			
			stmt.setString(1, i.getName());
			
			boo = stmt.executeUpdate() > 0;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean delete(int id) throws SQLException {
		PreparedStatement stmt = getDeleteStmt("category");
		stmt.setInt(1, id);

		return stmt.executeUpdate() > 0;
	}

	public int selectId(String n) {
		ResultSet rs = null;
		PreparedStatement stmt;
		try {
			stmt = getSelectIdStmt("category");
			
			stmt.setString(1, n);
			
			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(ID_INDEX);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean truncate() throws SQLException {
		PreparedStatement stmt = getTruncateStmt(table);
		
		conn.createStatement().executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
		boolean b = stmt.executeUpdate() == 0;
		conn.createStatement().executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
		
		return b;
	}
	
	public boolean resetId() throws SQLException {
		PreparedStatement stmt = getResetIdStmt("category");
		
		return stmt.executeUpdate() > 0;
	}
}
