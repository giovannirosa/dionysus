package controller.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import model.Item;
import model.Model;

public class DBItem extends Database {
	public static final int ID_INDEX = 1;
	public static final int NAME_INDEX = 2;
	public static final int VALUE_INDEX = 3;
	public static final int COMBO_INDEX = 4;
	
	PreparedStatement insertStmt;
	public PreparedStatement getInsertStmt() throws SQLException{
    	if( insertStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			insertStmt  = conn.prepareStatement("INSERT INTO item (name,value,combo) VALUE (?, ?, ?)");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return insertStmt;
    }
	
	PreparedStatement updateStmt;
	public PreparedStatement getUpdateStmt() throws SQLException{
    	if( updateStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			updateStmt  = conn.prepareStatement("UPDATE item SET name = ?, value = ?, combo = ? WHERE id = ?");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return updateStmt;
    }
	
	public int selectId(String n) {
		ResultSet rs = null;
		PreparedStatement stmt;
		try {
			stmt = getSelectIdStmt("item");
			
			stmt.setString(1, n);
			
			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(ID_INDEX);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean update(Model m) {
		Item i = (Item) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getUpdateStmt();
			
			stmt.setString(1, i.getName());
			stmt.setDouble(2, i.getValue());
			stmt.setBoolean(3, i.isCombo());
			stmt.setInt(4, i.getId());
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public Map<Integer,Model> listAll() {
		ResultSet rs = null;
		Map<Integer,Model> itemMap = new HashMap<>();
		PreparedStatement stmt;
		try {
			stmt = getSelectStmt("item");

			rs = stmt.executeQuery();
			while (rs.next()) {

				int id = rs.getInt(ID_INDEX);
				String name = rs.getString(NAME_INDEX);
				double value = rs.getDouble(VALUE_INDEX);
				boolean combo = rs.getBoolean(COMBO_INDEX);

				itemMap.put(id,new Item(id,name,value,combo));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return itemMap;
	}
	
	public boolean insert(Model m) {
		Item i = (Item) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getInsertStmt();
			
			stmt.setString(1, i.getName());
			stmt.setDouble(2, i.getValue());
			stmt.setBoolean(3, i.isCombo());
			
			boo = stmt.executeUpdate() > 0;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean delete(int id) throws SQLException {
		PreparedStatement stmt = getDeleteStmt("item");
		stmt.setInt(1, id);

		return stmt.executeUpdate() > 0;
	}
	
	public boolean truncate() throws SQLException {
		PreparedStatement stmt = getTruncateStmt("item");
		
		conn.createStatement().executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
		boolean b = stmt.executeUpdate() == 0;
		conn.createStatement().executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
		
		return b;
	}
	
	public boolean resetId() throws SQLException {
		PreparedStatement stmt = getResetIdStmt("item");
		
		return stmt.executeUpdate() > 0;
	}
}
