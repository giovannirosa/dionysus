package controller.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBParty {

	Connection conn = Connector.getConnection();
	
	PreparedStatement selectStmt;
	public PreparedStatement getSelectStmt() throws SQLException{
    	if( selectStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			selectStmt  = conn.prepareStatement("SELECT name FROM party WHERE id = 1");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return selectStmt;
    }
	
	PreparedStatement updateStmt;
	public PreparedStatement getUpdateStmt() throws SQLException{
    	if( updateStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			updateStmt  = conn.prepareStatement("UPDATE party SET name = ? WHERE id = 1");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return updateStmt;
    }
	
	PreparedStatement insertStmt;
	public PreparedStatement getInsertStmt() throws SQLException{
    	if( insertStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			insertStmt  = conn.prepareStatement("INSERT INTO party (name) value ('')");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return insertStmt;
    }
	
	PreparedStatement resetId;
	public PreparedStatement getResetIdStmt() throws SQLException{
    	if( resetId == null ){
    		if( conn != null && !conn.isClosed() )
    			resetId  = conn.prepareStatement("ALTER TABLE party AUTO_INCREMENT = 1");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return resetId;
    }
	
	public String select() {
		ResultSet rs = null;
		PreparedStatement stmt;
		try {
			stmt = getSelectStmt();
			
			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				stmt = getResetIdStmt();
				stmt.executeUpdate();
				
				stmt = getInsertStmt();
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public boolean update(String p) {
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getUpdateStmt();
			
			stmt.setString(1, p);
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
}
