package controller.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import model.Method;
import model.Model;

public class DBMethod extends Database {
	
	public static final int ID_INDEX = 1;
	public static final int NAME_INDEX = 2;
	public static final int LIMIT_INDEX = 3;
	
	public DBMethod() {
		table = "method";
	}
	
	PreparedStatement insertStmt;
	public PreparedStatement getInsertStmt() throws SQLException{
    	if( insertStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			insertStmt  = conn.prepareStatement("INSERT INTO method (name,limitVal) VALUE (?, ?)");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return insertStmt;
    }
	
	PreparedStatement updateStmt;
	public PreparedStatement getUpdateStmt() throws SQLException{
    	if( updateStmt == null ){
    		if( conn != null && !conn.isClosed() )
    			updateStmt  = conn.prepareStatement("UPDATE method SET name = ?, limitVal = ? WHERE id = ?");
    		else
    			throw new RuntimeException("Connection must be openned");
    	}
    	return updateStmt;
    }
	
	public boolean update(Model m) {
		Method i = (Method) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getUpdateStmt();
			
			stmt.setString(1, i.getName());
			stmt.setDouble(2, i.getLimit());
			stmt.setInt(3, i.getId());
			
			boo = stmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public Map<Integer,Model> listAll() {
		ResultSet rs = null;
		Map<Integer,Model> methodMap = new HashMap<>();
		PreparedStatement stmt;
		try {
			stmt = getSelectStmt("method");

			rs = stmt.executeQuery();
			while (rs.next()) {

				int id = rs.getInt(ID_INDEX);
				String name = rs.getString(NAME_INDEX);
				double limit = rs.getDouble(LIMIT_INDEX);

				methodMap.put(id,new Method(id,name,limit));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return methodMap;
	}
	
	public boolean insert(Model m) {
		Method i = (Method) m;
		PreparedStatement stmt;
		boolean boo = false;
		try {
			stmt = getInsertStmt();
			
			stmt.setString(1, i.getName());
			stmt.setDouble(2, i.getLimit());
			
			boo = stmt.executeUpdate() > 0;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return boo;
	}
	
	public boolean delete(int id) throws SQLException {
		PreparedStatement stmt = getDeleteStmt("method");
		stmt.setInt(1, id);

		return stmt.executeUpdate() > 0;
	}

	public int selectId(String n) {
		ResultSet rs = null;
		PreparedStatement stmt;
		try {
			stmt = getSelectIdStmt("method");
			
			stmt.setString(1, n);
			
			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(ID_INDEX);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean truncate() throws SQLException {
		PreparedStatement stmt = getTruncateStmt(table);
		
		conn.createStatement().executeUpdate("SET FOREIGN_KEY_CHECKS = 0");
		boolean b = stmt.executeUpdate() == 0;
		conn.createStatement().executeUpdate("SET FOREIGN_KEY_CHECKS = 1");
		
		return b;
	}
	
	public boolean resetId() throws SQLException {
		PreparedStatement stmt = getResetIdStmt("method");
		
		return stmt.executeUpdate() > 0;
	}
}
