package controller;

import java.util.HashMap;
import java.util.Map;

import controller.database.DBVip;
import javafx.collections.ObservableList;
import model.Vip;
import model.Model;
import model.table.TVipModel;
import model.table.TableModel;

public class VipControl extends Control {
	
	Map<Integer,Model> vipMap = new HashMap<>();
	DBVip dbVip = new DBVip();
	
	public VipControl() {
		setDatabase(dbVip);
		setMap(vipMap);
	}
	
	public void loadTable(ObservableList<TableModel> data) {		
		data.clear();
		for (Model m : vipMap.values()) {
			Vip i = (Vip) m;
			data.add(new TVipModel(i.getId(), i.getName(), i.getValue()));
		}
	}
}
