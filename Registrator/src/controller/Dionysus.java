package controller;

import javafx.application.Application;
import javafx.stage.Stage;
import view.LoginView;

public class Dionysus extends Application {

	public static void boot() {
		launch();
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		stage = new LoginView();
		stage.show();
	}
}
