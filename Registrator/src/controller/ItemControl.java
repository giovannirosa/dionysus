package controller;

import java.util.HashMap;
import java.util.Map;

import controller.database.DBItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import model.Item;
import model.Model;
import model.table.TItemModel;
import model.table.TableModel;

public class ItemControl extends Control {
	
	Map<Integer,Model> itemMap = new HashMap<>();
	DBItem dbItem = new DBItem();
	
	public ItemControl() {
		setDatabase(dbItem);
		setMap(itemMap);
	}
	
	public void refreshBoxCombo(ComboBox<String> box) {
		ObservableList<String> options = FXCollections.observableArrayList();
		for (Model i : map.values()) {
			if (!((Item)i).isCombo())
				options.add(i.getDesc());
		}
		box.setItems(options);
		box.getSelectionModel().select(0);
	}
	
	public void loadTable(ObservableList<TableModel> data) {		
		data.clear();
		if (itemMap.isEmpty()) {
			data.add(new TItemModel());
			return;
		}
		for (Model m : itemMap.values()) {
			Item i = (Item) m;
			data.add(new TItemModel(i.getId(), i.getName(), i.getValue(), i.isCombo()));
		}
	}
}
