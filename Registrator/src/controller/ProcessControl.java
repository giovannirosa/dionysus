package controller;

import java.util.Map;

import model.Item;
import model.Model;
import model.Payment;
import model.Sale;
import model.Storage;
import model.table.TProcModel;
import model.table.TStorageModel;
import model.table.TableModel;
import view.Screen;
import view.table.ProcessTable;
import view.table.StorageTable;
import view.utils.Factory;
import view.utils.Printer;

public class ProcessControl {
	
	public static void processSale(ProcessTable procTable, StorageTable storageTable,
			String method, double total) {
		for (TableModel m : storageTable.getItems()) {
			TStorageModel stModel = (TStorageModel) m;
			if (stModel.isChanged()) {
				Model genModel = new Storage(stModel.getId(),stModel.getItem(),
						stModel.getQuantity(),stModel.getCategory());
				Screen.storageControl.update(genModel);
			}
		}

		Map<Integer,Model> saleMap = Screen.saleControl.getMap();

		LOOP: for (TableModel m : procTable.getItems()) {
			TProcModel pModel = (TProcModel) m;
			int item = Screen.itemControl.getId(pModel.getItem());
			int quantity = Integer.parseInt(pModel.getQuantity());
			Item i = (Item) Screen.itemControl.get(item);
			for (Model saleModel : saleMap.values()) {
				Sale sale = (Sale) saleModel;
				if (sale.getItem() == item) {
					sale.setQuantity(sale.getQuantity()+quantity);
					sale.setTotal(quantity*i.getValue());
					Screen.saleControl.update(sale);
					String log = Screen.userControl.getUser().getName()+" sold "+quantity+" "+pModel.getItem();
					Factory.log(log);
					continue LOOP;
				}
			}

			Screen.saleControl.insert(new Sale(item,quantity,quantity*i.getValue()));
			String log = Screen.userControl.getUser().getName()+" sold "+quantity+" "+pModel.getItem();
			Factory.log(log);
		}
		
		Printer.clearBuffer();
		for (TableModel m : procTable.getItems()) {
			TProcModel pModel = (TProcModel) m;
			for (int i = 0; i < Integer.parseInt(pModel.getQuantity()); i++) {
				System.out.println(pModel.getItem());
				Printer.addBuffer(pModel.getItem());
			}
		}
		Printer.printBuffer();
		Printer.clearBuffer();
		
		Map<Integer,Model> payMap = Screen.payControl.getMap();
		int methodId = Screen.methodControl.getId(method);
		
		for (Model m : payMap.values()) {
			Sale p = (Sale) m;
			if (p.getItem() == methodId) {
				p.setQuantity(p.getQuantity()+1);
				p.setTotal(p.getTotal()+total);
				Screen.payControl.update(p);
				String log = Screen.userControl.getUser().getName()+" sold "+total+" in "+method;
				System.out.println(log);
				Factory.log(log);
				return;
			}
		}
		
		Screen.payControl.insert(new Payment(methodId,1,total));
		String log = Screen.userControl.getUser().getName()+" sold "+total+" in "+method;
		System.out.println(log);
		Factory.log(log);
	}
}
