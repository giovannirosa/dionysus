package controller;

import java.util.HashMap;
import java.util.Map;

import controller.database.DBSale;
import javafx.collections.ObservableList;
import model.Item;
import model.Model;
import model.Sale;
import model.table.TSaleModel;
import model.table.TableModel;
import view.Screen;

public class SaleControl extends Control {

	Map<Integer,Model> saleMap = new HashMap<>();
	DBSale dbSale = new DBSale();
	
	public SaleControl() {
		setDatabase(dbSale);
		setMap(saleMap);
	}
	
	public void loadTable(ObservableList<TableModel> data) {		
		data.clear();
		if (saleMap.isEmpty()) {
			data.add(new TSaleModel());
			return;
		}
		for (Model m : saleMap.values()) {
			Sale o = (Sale) m;
			Item i = (Item) Screen.itemControl.get(o.getItem());
			data.add(new TSaleModel(o.getId(), i.getName(), o.getQuantity(),
					i.getValue()*o.getQuantity(), i.getValue()));
		}
	}
}
