package controller;

import java.util.HashMap;
import java.util.Map;

import controller.database.DBStorage;
import javafx.collections.ObservableList;
import model.Category;
import model.Item;
import model.Model;
import model.Storage;
import model.table.TStorageModel;
import model.table.TableModel;
import view.Screen;

public class StorageControl extends Control {

	Map<Integer,Model> storageMap = new HashMap<>();
	DBStorage dbStorage = new DBStorage();
	
	public StorageControl() {
		setDatabase(dbStorage);
		setMap(storageMap);
	}
	
	public void loadTable(ObservableList<TableModel> data) {		
		data.clear();
		if (storageMap.isEmpty()) {
			data.add(new TStorageModel());
			return;
		}
		for (Model m : storageMap.values()) {
			Storage o = (Storage) m;
			Item i = (Item) Screen.itemControl.get(o.getItem());
			Category c = (Category) Screen.categoryControl.get(o.getCategory());
			data.add(new TStorageModel(o.getId(), i.getName(), 
					o.getQuantity(), i.getValue(), c.getName()));
		}
	}
}
