package controller;

import java.util.HashMap;
import java.util.Map;

import controller.database.DBUser;
import javafx.collections.ObservableList;
import model.Model;
import model.User;
import model.table.TUserModel;
import model.table.TableModel;

public class UserControl extends Control {

	private Map<Integer,Model> userMap = new HashMap<>();
	private DBUser dbUsers = new DBUser();
	
	private static User user;
	
	public UserControl() {
		setDatabase(dbUsers);
		setMap(userMap);
	}
	
	public void setUser(User user) {
		UserControl.user = user;
	}
	
	public User getUser() {
		return user;
	}
	
	public void loadTable(ObservableList<TableModel> data) {		
		data.clear();
		if (userMap.isEmpty()) {
			data.add(new TUserModel());
			return;
		}
		for (Model m : userMap.values()) {
			User u = (User) m;
			data.add(new TUserModel(u.getId(), u.getName(), u.getPass(), u.getRole().toString()));
		}
	}
}
